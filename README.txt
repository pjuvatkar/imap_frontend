Upon register all data can be sent to the server by POSTing the data. 'Asynctasks' can be used for posting the data.

If the person logs in once, 'sharedPrefs' are used to remember his login, he needn't login again on the same device.
He can only log out. Log in authentication could happen via server.

All help buttons in menu lead to same activity Help.java.

Menu on MapActivity-All linked.
	Activity for Settings, Search, Event Details activity.
	Help button leads to same activity Help.java.
	MyLoc and Directions buttons linked.