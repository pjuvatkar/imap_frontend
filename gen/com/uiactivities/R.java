/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.uiactivities;

public final class R {
    public static final class attr {
    }
    public static final class color {
        public static final int light_gray=0x7f060000;
    }
    public static final class drawable {
        public static final int arrow24=0x7f020000;
        public static final int arrow48=0x7f020001;
        public static final int blank24=0x7f020002;
        public static final int blank48=0x7f020003;
        public static final int circleb_s=0x7f020004;
        public static final int circleg_s=0x7f020005;
        public static final int circlegw=0x7f020006;
        public static final int circlegwl=0x7f020007;
        public static final int finalloc=0x7f020008;
        public static final int flag=0x7f020009;
        public static final int flag2_50x50=0x7f02000a;
        public static final int flag2_small=0x7f02000b;
        public static final int ic_launcher=0x7f02000c;
        public static final int myloc=0x7f02000d;
        public static final int person=0x7f02000e;
        public static final int person_small=0x7f02000f;
        public static final int picture1=0x7f020010;
        public static final int picture2=0x7f020011;
        public static final int picture3=0x7f020012;
        public static final int picture4=0x7f020013;
        public static final int selector=0x7f020014;
        public static final int vgapp=0x7f020015;
        public static final int vibrantgujarat_logo=0x7f020016;
    }
    public static final class id {
        public static final int LinearLayout1=0x7f08003b;
        public static final int RadioButton0=0x7f08000f;
        public static final int RelativeLayout1=0x7f080002;
        public static final int ScrollView01=0x7f08003a;
        public static final int TableLayout01=0x7f080011;
        public static final int TableRow0=0x7f08001e;
        public static final int TableRow01=0x7f080012;
        public static final int TableRow02=0x7f08001b;
        public static final int TableRow03=0x7f080018;
        public static final int TableRow04=0x7f080015;
        public static final int button1=0x7f080007;
        public static final int button2=0x7f08000d;
        public static final int cancel=0x7f080035;
        public static final int canceldialog=0x7f08003d;
        public static final int checkBox1=0x7f080037;
        public static final int checkBoxSetRem=0x7f080034;
        public static final int demo=0x7f08000b;
        public static final int desc=0x7f080032;
        public static final int dismiss=0x7f080039;
        public static final int done=0x7f080036;
        public static final int duration=0x7f08002f;
        public static final int email=0x7f080017;
        public static final int email_text=0x7f080016;
        public static final int end_demo=0x7f080001;
        public static final int errmsg=0x7f080022;
        public static final int imageView1=0x7f080008;
        public static final int image_view=0x7f080025;
        public static final int listView1=0x7f080005;
        public static final int loc=0x7f08002c;
        public static final int mapLayout=0x7f08000c;
        public static final int menu_settings=0x7f08003e;
        public static final int mobileNo=0x7f08001a;
        public static final int mobileNo_text=0x7f080019;
        public static final int name=0x7f08002a;
        public static final int password=0x7f08001d;
        public static final int password2=0x7f080020;
        public static final int password_text=0x7f08001c;
        public static final int password_text2=0x7f08001f;
        public static final int path=0x7f080038;
        public static final int pwd=0x7f080024;
        public static final int radioButton1=0x7f080010;
        public static final int regbutton=0x7f08000a;
        public static final int regdesc=0x7f080028;
        public static final int searchbar=0x7f080023;
        public static final int settingsdialogcontent=0x7f08003c;
        public static final int signinbutton=0x7f080009;
        public static final int spinner1=0x7f080004;
        public static final int submitregbutton=0x7f080021;
        public static final int tableLayout1=0x7f080026;
        public static final int tableRow01=0x7f080027;
        public static final int tableRow1=0x7f080029;
        public static final int tableRow2=0x7f08002b;
        public static final int tableRow3=0x7f08002d;
        public static final int tableRow4=0x7f080030;
        public static final int tableRow5=0x7f080033;
        public static final int textView1=0x7f080003;
        public static final int textView2=0x7f080006;
        public static final int textView3=0x7f08002e;
        public static final int textView4=0x7f080031;
        public static final int toggleGroup=0x7f08000e;
        public static final int userName=0x7f080014;
        public static final int username_text=0x7f080013;
        public static final int view_pager=0x7f080000;
    }
    public static final class layout {
        public static final int activity_demo=0x7f030000;
        public static final int activity_eventdetails=0x7f030001;
        public static final int activity_help=0x7f030002;
        public static final int activity_loctap=0x7f030003;
        public static final int activity_main=0x7f030004;
        public static final int activity_map=0x7f030005;
        public static final int activity_reg=0x7f030006;
        public static final int activity_searchloc=0x7f030007;
        public static final int activity_settings=0x7f030008;
        public static final int activity_signin=0x7f030009;
        public static final int demo_single_view=0x7f03000a;
        public static final int dialog_eventregister=0x7f03000b;
        public static final int dialog_eventreminder=0x7f03000c;
        public static final int dialog_reminder=0x7f03000d;
        public static final int dialog_settings=0x7f03000e;
    }
    public static final class menu {
        public static final int activity_main=0x7f070000;
        public static final int activity_map=0x7f070001;
    }
    public static final class string {
        public static final int _0=0x7f040009;
        public static final int _1=0x7f04000a;
        public static final int app_name=0x7f040000;
        public static final int cancel=0x7f04000f;
        public static final int description=0x7f040014;
        public static final int done=0x7f040012;
        public static final int duration=0x7f040011;
        public static final int get_directions_button=0x7f04000c;
        public static final int hello_world=0x7f040001;
        public static final int help=0x7f04000d;
        public static final int large_text=0x7f040010;
        public static final int location=0x7f040008;
        public static final int main_logo=0x7f040006;
        public static final int menu_settings=0x7f040002;
        public static final int my_location_button=0x7f04000b;
        public static final int name=0x7f040015;
        public static final int no=0x7f040018;
        public static final int okay=0x7f04000e;
        public static final int register=0x7f040004;
        public static final int search=0x7f040007;
        public static final int set_reminder=0x7f040016;
        public static final int sign_in=0x7f040005;
        public static final int sort_by=0x7f040003;
        public static final int textview=0x7f040013;
        public static final int yes=0x7f040017;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f050000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f050001;
    }
}
