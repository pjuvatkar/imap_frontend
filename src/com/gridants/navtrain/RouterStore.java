package com.gridants.navtrain;

public class RouterStore implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	public int[][][] RouterLevel;
	public int[][][] RouterId;
	
	public RouterStore(int i, int j, int k){
		RouterLevel = new int[i][j][k];
		RouterId = new int[i][j][k];
	}
}
