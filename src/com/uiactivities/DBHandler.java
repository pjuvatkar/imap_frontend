 package com.uiactivities;

import java.io.IOException;

import com.gridants.navtrain.RouterStore;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DBHandler {
	
	// Database
	private SQLiteDatabase database;
	private MySQLiteHelper dbHelper;
	
	public DBHandler(Context context, String name) {
		dbHelper = new MySQLiteHelper(context, name);
		Log.i("Test","Inside DBHandler: createdDatabse");
		
		try {
			 
			dbHelper.createDataBase();
 
	 	} catch (IOException ioe) {
	 
	 		throw new Error("Unable to create database");
	 
	 	}
	}
	
	public void open() throws SQLException {
		//database = dbHelper.getWritableDatabase();
		
		try {
			 
	 		database = dbHelper.openDataBase();
	 
	 	}catch(SQLException sqle){
	 
	 		throw sqle;
	 
	 	}
		
		//database1 = SQLiteDatabase.
		Log.i("Test","Inside DBHandler: Opened database");
		
		// Create readings table
		String CREATE_READINGS_TABLE = "CREATE TABLE IF NOT EXISTS " + MySQLiteHelper.TABLE_READINGS + "("
				+ MySQLiteHelper.COLUMN_LOCATION + " INT," + MySQLiteHelper.COLUMN_READINGNUM + " INT," 
				+ MySQLiteHelper.COLUMN_ROUTERID + " INT," +  MySQLiteHelper.COLUMN_LEVEL + " INT" +" )";
		database.execSQL(CREATE_READINGS_TABLE);
		Log.i("Database","Created readings table");
		
		// create routers table
		String CREATE_ROUTERS_TABLE = "CREATE TABLE IF NOT EXISTS " + MySQLiteHelper.TABLE_ROUTERS + " (" 
				+ MySQLiteHelper.COLUMN_ROUTERID + " INTEGER PRIMARY KEY AUTOINCREMENT," 
				+ MySQLiteHelper.COLUMN_BSSID +" text)";
		database.execSQL(CREATE_ROUTERS_TABLE);
		Log.i("Database","Created routers table");
		
		// create map table
		String CREATE_MAP_TABLE = "CREATE TABLE IF NOT EXISTS " + MySQLiteHelper.TABLE_MAP + " (" 
				+ MySQLiteHelper.COLUMN_LOCATION + " INTEGER PRIMARY KEY AUTOINCREMENT," 
				+ MySQLiteHelper.COLUMN_XLAT +" INT, " + MySQLiteHelper.COLUMN_YLON +" INT)";
		database.execSQL(CREATE_MAP_TABLE);
		Log.i("Database","Created map table");

		// create path table
		String CREATE_PATH_TABLE = "CREATE TABLE IF NOT EXISTS " + MySQLiteHelper.TABLE_PATH + " (" 
				+ MySQLiteHelper.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + MySQLiteHelper.COLUMN_XLAT +" INT, " + MySQLiteHelper.COLUMN_YLON +" INT)";
		database.execSQL(CREATE_PATH_TABLE);
		Log.i("Database","Created path table");
	}
	
	public void close() {
		dbHelper.close();
		Log.i("Test","Inside DBHandler: Closed database");
		Log.i("Database","Inside DBHandler: Closed database");
	}
	
	public boolean deleteInternal() {
		try {
			return dbHelper.deleteDataBase();
		} catch (IOException e) {
			return false;
		}
	}
	
	public void upgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		dbHelper.onUpgrade(db, oldVersion, newVersion);
	}
	
	public RouterStore dbtoarray(int index3){
		
		// Get the total number of locations from MAP table
		
		String sizeQuery = "SELECT MAX(" + MySQLiteHelper.COLUMN_LOCATION + ") FROM " + MySQLiteHelper.TABLE_MAP;
		//Log.i("Database","Checking size of map table - Query: " + sizeQuery);
		Cursor cursor_sizemap = database.rawQuery(sizeQuery, null);
		cursor_sizemap.moveToFirst();
		// Index 1 is the size of first dimension of the array
		int index1 = cursor_sizemap.getInt(0);
		cursor_sizemap.close();
		
		// Get the maximum number of readings for a location
		
		sizeQuery = "SELECT MAX(r) FROM (SELECT COUNT(DISTINCT " + MySQLiteHelper.COLUMN_READINGNUM + ") AS r FROM " 
				+ MySQLiteHelper.TABLE_READINGS + " GROUP BY " + MySQLiteHelper.COLUMN_LOCATION + ") AS t1";
		cursor_sizemap = database.rawQuery(sizeQuery, null);
		cursor_sizemap.moveToFirst();
		int index2 = cursor_sizemap.getInt(0);
		cursor_sizemap.close();
		
		// Get the number of BSSID's to incorporate in the array
		// Size of third dimension of the array will be index3
		RouterStore router = new RouterStore(index1, index2, index3);
		
		// Create the 3-D arrays
//		int [][][] RouterId = new int[index1][index2][index3];
//		int [][][] RouterLevel = new int[index1][index2][index3];
		
		int limit = 10000/200;
		int start = 0;
		int end = limit;
		if(end > index1)
			end = index1;
		int prevI = 0;
		
		while( start < index1 ) {
			Log.i("Test"," Fetching rows from " + start + " to " + end);
			String selectQuery = "SELECT " + MySQLiteHelper.COLUMN_LOCATION + "," + MySQLiteHelper.COLUMN_READINGNUM + "," +
					MySQLiteHelper.COLUMN_ROUTERID + "," + MySQLiteHelper.COLUMN_LEVEL + " FROM " + MySQLiteHelper.TABLE_READINGS + 
					" WHERE " + MySQLiteHelper.COLUMN_LOCATION + ">" + start +" AND " + MySQLiteHelper.COLUMN_LOCATION + "<=" + end;
			Cursor cursor = database.rawQuery(selectQuery, null);
			
			cursor.moveToFirst();
			
			// Here we need to ensure that the levels for a reading are in sorted order
			
			// Here we are adding the readings in the arrays
			// i tracks the third index
			int i = prevI;
			int readingprev = cursor.getInt(1);
			if (cursor.moveToFirst()){
				do {
					// If the reading number changes, reset i to 0
					if (readingprev != cursor.getInt(1)){
						i = 0;
					}
					// Update the previous reading number
					readingprev = cursor.getInt(1);
					
					// If the routers exceed third dimension of the array, reset i to 0 and skip remaining routers in that reading
					if (i >= index3){
						//Log.e("Test"," Skipped " + cursor.getInt(0) + " Reading No " + cursor.getInt(1) + " Router Id " + i + " Level " + cursor.getInt(3));
						i++;
						continue;
					}
					
					// Add the reading in the arrays
					router.RouterId[cursor.getInt(0) - 1][cursor.getInt(1) - 1][i] = cursor.getInt(2) - 1;
					router.RouterLevel[cursor.getInt(0) - 1][cursor.getInt(1) - 1][i] = cursor.getInt(3);
					
					//Log.i("Test"," Location " + cursor.getInt(0) + " Reading No " + cursor.getInt(1) + " Router Id " + i + " Level " + cursor.getInt(3));
					// Increment the third index of the router
					i++;
					
				} while (cursor.moveToNext());
				prevI = i;
				Log.i("Test"," Previous Router Id " + prevI);
			}
			cursor.close();
			
			// Updating interval boundary
			start = end;
			end = end + limit;
			if(end > index1)
				end = index1;
		}
		Log.i("Test"," Fetched total " + index1 + " rows" );
		return router;
	}
	
	public int getRoutersNum(){
		String sizeQuery = "SELECT MAX(" + MySQLiteHelper.COLUMN_ROUTERID + ") FROM " + MySQLiteHelper.TABLE_ROUTERS;
		Cursor cursor_sizemap = database.rawQuery(sizeQuery, null);
		cursor_sizemap.moveToFirst();
		int RoutersNum = cursor_sizemap.getInt(0);
		cursor_sizemap.close();
		return RoutersNum;
	}
	
	public int getRouterId(String BSSID){
		String sizeQuery = "SELECT " + MySQLiteHelper.COLUMN_ROUTERID + " FROM " + MySQLiteHelper.TABLE_ROUTERS + " WHERE " + MySQLiteHelper.COLUMN_BSSID + " = \"" + BSSID + "\"";
		Cursor cursor_sizemap = database.rawQuery(sizeQuery, null);
		cursor_sizemap.moveToFirst();
		int RoutersNum = cursor_sizemap.getInt(0) - 1;
		cursor_sizemap.close();
		return RoutersNum;
	}

	public String[] getRouterArray() {
		int routerCount = getRoutersNum();
		String[] routerArray = new String[routerCount];
		
		String Query = "SELECT " + MySQLiteHelper.COLUMN_ROUTERID + "," + MySQLiteHelper.COLUMN_BSSID + " FROM " + MySQLiteHelper.TABLE_ROUTERS;
		Cursor cursor = database.rawQuery(Query, null);
		cursor.moveToFirst();
		
		if (cursor.moveToFirst()){
			do {
				routerArray[cursor.getInt(0)-1] = cursor.getString(1);
			} while (cursor.moveToNext());
		}
		cursor.close();		
		return routerArray;
	}
}
