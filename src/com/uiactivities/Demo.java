package com.uiactivities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class Demo extends Activity {

	private static final int MAX_VIEWS = 6;

	ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_demo);
		mViewPager = (ViewPager) findViewById(R.id.view_pager);
		mViewPager.setAdapter(new WalkthroughPagerAdapter());
		mViewPager.setOnPageChangeListener(new WalkthroughPageChangeListener());
		TextView end=(TextView) findViewById(R.id.end_demo);
		end.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	class WalkthroughPagerAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			return MAX_VIEWS;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == (View) object;
		}

		@Override
		public Object instantiateItem(View container, int position) {
			Log.e("walkthrough", "instantiateItem(" + position + ");");
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View imageViewContainer = inflater.inflate(
					R.layout.demo_single_view, null);
			ImageView imageView = (ImageView) imageViewContainer
					.findViewById(R.id.image_view);

			switch (position) {
			case 0:
				imageView.setImageResource(R.drawable.picture1);
				break;

			case 1:
				imageView.setImageResource(R.drawable.picture2);
				break;

			case 2:
				imageView.setImageResource(R.drawable.picture3);
				break;

			case 3:
				imageView.setImageResource(R.drawable.picture4);
				break;
				
			case 4:
				//imageView.setImageResource(R.drawable.picture4);
				break;
			case 5:
				//imageView.setImageResource(R.drawable.picture4);
				finish();
				break;
			
			}
			
			

			((ViewPager) container).addView(imageViewContainer, 0);
			return imageViewContainer;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((View) object);
		}
	}

	class WalkthroughPageChangeListener implements
			ViewPager.OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int arg0) {

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {

		}

		@Override
		public void onPageSelected(int position) {
			// Here is where you should show change the view of page indicator
			switch (position) {

			case MAX_VIEWS - 1:
				break;

			default:

			}
		}
	}
}
