package com.uiactivities;

import static com.uiactivities.MapActivity.e;
import static com.uiactivities.MapActivity.locationNames;
import static com.uiactivities.MapActivity.mapActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class EventDetails extends Activity {

	public static String[] newname;
	public static int[] newloc;
	public static int[] newduration;
	public static String[] newstarttime;
	public int position;
	public static List<Integer> q1;
	
	public Dialog dialog=null;
	
	ArrayAdapter<String> adapter ;
	List<Map<String, String>> eventslist;
	private MenuItem Update;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_eventdetails);
		
		if(q1==null)
			q1=new ArrayList<Integer>();
		((MapActivity) mapActivity).getEventObj();
		
		List<String> sortbylist = new ArrayList<String>();
		sortbylist.add("Time");
		sortbylist.add("Name");
		sortbylist.add("Distance");
		 // or whatever required
		
		if(e==null)
			return;
		
		newname=new String[e.size()];
		newloc=new int[e.size()];
		newduration=new int[e.size()];
		newstarttime=new String[e.size()];
		
		ArrayAdapter<String> sortspinneradapter = new ArrayAdapter<String>(
				this, android.R.layout.simple_spinner_item, sortbylist);
		sortspinneradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Spinner s = (Spinner) findViewById(R.id.spinner1);
		s.setAdapter(sortspinneradapter);
		s.setOnItemSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				String item = ((TextView) arg1).getText().toString();
				Toast.makeText(getBaseContext(), item, Toast.LENGTH_SHORT).show();
				if(item.equals("Name")){
					for (int j = 0; j < e.size(); j++) {
						newname[j]=e.get(j).Name;
					}
//					newname=name.clone();
					Arrays.sort(newname);
					for (int i = 0; i < newname.length; i++) {
						for (int j = 0; j < e.size(); j++) {
							if(newname[i].equals(e.get(j).Name)){
								newloc[i]=e.get(j).LocationID;
								newduration[i]=e.get(j).Duration;
								newstarttime[i]=e.get(j).StartTime;
							}
						}
					}
					getEventsList();
				}
				
				else if(item.equals("Distance")){
					int[] dist=new int[e.size()];
					for (int i = 0; i < e.size(); i++) {
						dist[i]=getCurrentDistance(e.get(i).LocationID);
					}
					int[] newdist;
					newdist=dist.clone();
					Arrays.sort(newdist);
					List<Integer> j1=new ArrayList<Integer>();
					for (int i = 0; i < newdist.length; i++) {
						for (int j = 0; j < dist.length; j++) {
							if(!j1.contains(j)&&newdist[i]==dist[j]){
								newname[i]=e.get(j).Name;
								newloc[i]=e.get(j).LocationID;
								newduration[i]=e.get(j).Duration;
								newstarttime[i]=e.get(j).StartTime;
								i++;
								if(i==newdist.length)
									break;
								j1.add(j);
								j=0;
							}
						}
					}
					getEventsList();
				}
				else if(item.equals("Time")){
                     
					String[] time=new String[e.size()];
					for (int i = 0; i < e.size(); i++) {
						time[i] = e.get(i).StartTime;						
						//time[i]=Integer.parseInt(x.substring(x.indexOf(" ")+1,x.indexOf(":")));
//						Log.d("hello",time[i]+"");
					}
					String[] newtime;
					newtime=time.clone();
					Arrays.sort(newtime);
					
					List<Integer> j1=new ArrayList<Integer>();
					
					for (int i = 0; i < newtime.length; i++) {
						for (int j = 0; j < time.length; j++) {
							if(!j1.contains(j)&&newtime[i]==time[j])
							{
								newname[i]=e.get(j).Name;
								newloc[i]=e.get(j).LocationID;
								newduration[i]=e.get(j).Duration;
								newstarttime[i]=e.get(j).StartTime;
								j1.add(j);
								break;
							}
						}
					}
					getEventsList();
				}
			}

			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		super.onCreateOptionsMenu(menu);
		
		Update = menu.add("Update");
	
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item == Update){
			((MapActivity) mapActivity).new UpdateEvents().execute();
			
			if(MapActivity.auth==false)
				return true;
			Intent i=new Intent(EventDetails.this, EventDetails.class);
			startActivity(i);
			finish();
		}
		return true;
	}
	
	int getCurrentDistance(int location) {
		return location * 10;
	}
	
	void getEventsList(){
		eventslist = new ArrayList<Map<String, String>>();
		for (int i = 0; i < newname.length; i++) {
			Map<String, String> datum = new HashMap<String, String>(2);
			try {
				// Parsing string to date
				Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).parse(newstarttime[i]);
				// Add only future events to event list
				if(date.after(new Date())) {
					datum.put("Name", newname[i]);
					datum.put("Desc", "Location:\t" + locationNames[newloc[i]-1] + "\n"
							+ "Duration:\t" +  String.format("%.1f", ((float)newduration[i])/60.0f) + " hours\n" 
							+ "Start Time:\t" +  new SimpleDateFormat("MMM dd KK:mm a", Locale.US).format(date) + "\n" 
							//+ "Distance:\t"
							//+ getCurrentDistance(newloc[i]) + " m"
							);
					eventslist.add(datum);
				}
			} catch (Exception e) {
				Log.e("Conversion", e.toString());
			}
		}
		
		ListView listview = (ListView) findViewById(R.id.listView1);
		SimpleAdapter adapter = new SimpleAdapter(this, eventslist,
				android.R.layout.simple_list_item_2, new String[] { "Name",
						"Desc" }, new int[] { android.R.id.text1,
						android.R.id.text2 });
		listview.setAdapter(adapter);

		listview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int pos, long id) {
				
				position=pos;
				
				String item = newname[position];
//				Toast.makeText(getBaseContext(), item, Toast.LENGTH_SHORT).show();
				int rp=0;
				for (int j = 0; j < e.size(); j++) {
					if(item.equals(e.get(j).Name))
						rp=j;
				}
				dialog = new Dialog(EventDetails.this);
//				enter(dialog, position);
				RegisterReminder r= new RegisterReminder();
				r.enter(dialog, rp, mapActivity);
			}
		});
	}
	
	
	@Override
	public void onStop(){
		super.onStop();
		//taskHandler.removeCallbacks(t);
	}
	
	protected Handler taskHandler = new Handler();
	Runnable t;
	public void setLocalRem(long time) {
		final long elapse = time;
		t = new Runnable() {
			public void run() {
				EventAlert();
			}
		};
		taskHandler.postDelayed(t, elapse);
	}

	protected void EventAlert() {
//		Toast.makeText(getBaseContext(), "Rem...", Toast.LENGTH_SHORT).show();
		
		Intent i = new Intent(getBaseContext(), Reminder.class);
		startActivity(i);
		Log.d("hello","rem");
	}
	
}
