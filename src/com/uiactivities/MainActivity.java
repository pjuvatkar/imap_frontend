package com.uiactivities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {
	
	private MenuItem Settings;
	private Button signin, register, demo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// Show the Databases stored in the assets folder to select the database to use
        AssetManager assetManager = getAssets();
        
        // Copy osmdroid zip file in sd card from assets if not already there
//        Misc m = new Misc();
        
        Log.i("Test","Started copying map");
        Misc.copyMap(assetManager, "bothfloorshifted.zip");
        Log.i("Test","Done copoying map");
        
		signin = (Button) findViewById(R.id.signinbutton);
		signin.setOnClickListener(this);
		register = (Button) findViewById(R.id.regbutton);
		register.setOnClickListener(this);
		demo = (Button) findViewById(R.id.demo);
		demo.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		super.onCreateOptionsMenu(menu);
		
		Settings = menu.add("Setting");
	
		return true;
	}
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item == Settings){
			Intent i=new Intent(this, Setting.class);
			startActivity(i);
		}	
		return true;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.signinbutton) {
			Intent i=new Intent(this, Signin.class);
			startActivity(i);
		}
		else if (v.getId() == R.id.regbutton) {
			Intent i=new Intent(this, Register.class);
			startActivity(i);
		}
		else if (v.getId() == R.id.demo) {
			Intent i=new Intent(this, Demo.class);
			startActivity(i);
		}
	}
	
	@Override
	public void onBackPressed() {
		Intent i = new Intent(Intent.ACTION_MAIN);
		i.addCategory(Intent.CATEGORY_HOME);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);
	}
}
