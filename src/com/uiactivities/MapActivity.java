package com.uiactivities;

import static com.uiactivities.Signin.PREFS_NAME;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.Overlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.PathOverlay;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.NavUtils;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gridants.navtrain.RouterStore;

public class MapActivity extends Activity implements OnClickListener, SensorEventListener{
	
	// Wi-Fi manager
	static WifiManager wifi;
	
	// Database handler object
	static DBHandler DbHandler;
	
	// Mode for scanning wifi only once
	public static int mode = 0;
	
	static RouterStore router;
	static String[] BSSIDList;
	public static boolean[][] RouterLocation;
	public static int[][] map;
	public static int[][] path;
	
	// Linked list for low pass filtering action
	static LinkedList<Integer> mLinkedList = new LinkedList<Integer>();

	// Maximum number of routers allowed per location- size of 3-D array
	public static int num_of_routers = 10;

	private final String PREFS_MYEVENTS = "myEventsPrefsFile";

	public static String myEventsString=null;
	public static String[] myEvents=null;
	
	public static Handler handler;
	
	// Manager to get the files in assets folder
	AssetManager mgr;
		
	private MenuItem Search, MyLoc, EventDet, Directions, Setting, Logout, MyEvents;
	
	ImageButton DestinationButton;
	ImageButton myLocButton;
	
	RadioGroup floorGroup;
	
	TextView t1;
	TextView t2;
	
	RadioButton radioBtn0;
	RadioButton radioBtn1;
	
	//MapView mapView;
	BoundedMapView mapView;
	
	int lat = 0000000;
	int lon = 0000000;
		
	static double x = 0;
	static double y = 0;
	
	double x_kwnn = 0;
	double y_kwnn = 0;
	
	int currentLocation;
	int filteredLocation=-1;
	int nearestLocation;
	
	// For index of location in training data, map it to universal points in path
	// Size of array will be equal to number of training points
	int[] filteredtonearest;
	
	int[][] floorchange = {
			{4,119},
			{100,102},
			{101,111},
			//{10, 132},
		};
	
	public static int finalLocation;
	
	int[][] IncidenceMatrix;

	static int[] locationXYtoMap = {
		1, 2, 4, 6, 10, 13, 14, 16, 18, 37, 46, 47, 50, 53,
		54, 55, 59, 60, 63, 64, 67, 70, 71, 72,
		83, 85, 86, 
		74, 75,
		
		129, 131, 133, 135, 137, 138, 139, 140, 141,
		109, 110,
		113,
	};
	
	boolean[] isLabelled = {
			/* Ground Floor begins */
			// Convention Centre & Exhibition Hall 3
			false, //1
			true, //2
			false, //4
			false, //6
			false, //10
			true, //13
			false, //14
			false, //16
			false, //18
			//"Staircase 2" //near 24
			true, //37
			true, //46
			false, //47
			true, //50
			true, //53
			
			// Exhibition Hall 1 & 2
			false, //54
			true, //55
			true, //59
			true, //60
			true, //63
			true, //64
			true, //67
			true, //70
			true, //71
			false, //72
			
			// Photo Gallery
			true, //83
			true, //85
			true, //86		
			
			// Food Court
			true, //74
			true, //75
			
			/* Ground Floor ends */
			
			/* First Floor begins */
			// Convention Centre & Exhibition Hall 3
			false, //129
			false, //131
			false, //133
			true, //135
			true, //137
			false, //138
			false, //139
			false, //140
			false, //141

			// Photo Gallery
			true, //109
			true, //110
			
			// Food Court
			true, //113
			/* First Floor ends */
	};
	
	static String[] locationNames = {
		/* Ground Floor begins */
		// Convention Centre & Exhibition Hall 3
		"Mahatma Mandir Entrance", //1
		"Reception of Mahatama Mandir", //2
		"Staircase 3", //4
		"Lift Lobby", //6
		"Staircase 1", //10
		"VIP Lounge", //13
		"Foyer 3", //14
		"Foyer 2", //16
		"Foyer 1", //18
		//"Staircase 2" //near 24
		"Main Convention Hall", //37
		"Silver Dining", //46
		"Exhibition Hall 3 Entrance", //47
		"Seminar Hall 1", //50
		"Seminar Hall 2", //53
		
		// Exhibition Hall 1 & 2
		"Exhibition Hall 1 Entrance Lobby", //54
		"Exhibition Hall 1", //55
		"Seminar Hall 6", //59
		"Seminar Hall 5", //60
		"Seminar Hall 7", //63
		"Seminar Hall 8", //64
		"Media Lounge", //67
		"Exhibition Hall 2", //70
		"Red and Blue Dining", //71
		"Exhibition Hall 2 Entrance Lobby", //72
		
		// Photo Gallery
		"Gold Dining", //83
		"CM Lounge", //85
		"Photo Gallery on Mahatma Gandhiji", //86		
		
		// Food Court
		"Food Court", //74
		"SIR Experience Centre", //75
		
		/* Ground Floor ends */
		
		/* First Floor begins */
		// Convention Centre & Exhibition Hall 3
		"First Floor Lift Lobby", //129
		"Conference Room 1", //131
		"Conference Room 3", //133
		"Seminar Hall 4", //135
		"Seminar Hall 3", //137
		"VIP Viewing Area 3", //138
		"Conference Room 2", //139
		"VIP Viewing Area 1", //140
		"VIP Viewing Area 2", //141

		// Photo Gallery
		"Partner Organizations Lounge", //109
		"CM Lounge First Floor", //110
		
		// Food Court
		"Food Court First Floor", //113
		/* First Floor ends */
	};
	
	public static List<Event> e;
	private static String TIME_STAMP = null;
	static boolean auth = false;
	boolean pollStatus = false, doPoll = true;
	private String PREFS_EVENT_TIMESTAMP = "timeStampPrefs";
	private String PREFS_EVENTS_DATA = "eventsDataPrefs";
	public static Activity mapActivity;
	public boolean doEvntUpdt=true, sendLoc=true;
	public long updtTime=300000, pollTime=60000;

	int lastFilteredLocation=1000;
	
	private SensorManager mSensorManager;
	private Sensor mAccelerometer;
	private Sensor mMagnetometer;
	
	private float[] mLastAccelerometer = new float[3];
    private float[] mLastMagnetometer = new float[3];
    private boolean mLastAccelerometerSet = false;
    private boolean mLastMagnetometerSet = false;
    
    private float[] mR = new float[9];
    private float[] mOrientation = new float[3];
    
    float azimuth;
	final float rad2deg = (float)(180.0f/Math.PI);
	
	double c_y;
	double c_x;
	double f_y;
	double f_x;
	
	MyItemizedIconOverlay mMyOverlay4;
	boolean isPositionUpdating = true;

	public Drawable drawable1, drawable2;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        
        getEventObj();
        
        mapActivity=MapActivity.this;
        
        thread1();
        new SplMsgPoll().execute();
        
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        
        finalLocation = locationXYtoMap[0] - 1;
        
        currentLocation = locationXYtoMap[0] - 1;
    	
        // filteredLocation is taken from points from training
        filteredLocation = locationXYtoMap[0] - 1; 
        
        drawable1 = MapActivity.this.getResources().getDrawable(R.drawable.person_small);
        drawable2 = MapActivity.this.getResources().getDrawable(R.drawable.flag2_50x50);
        
        handler = new Handler();
        mgr = getBaseContext().getAssets();
        
//        mapView.getController().animateTo(0.0,0.0);
        Log.i("Test"," Loading started router ");
   	  	router = (RouterStore) deserialize("readings.ser");
   	  	Log.i("Test"," Data loaded into memory " + router.RouterId.length + "  " + router.RouterId[0].length + "  " + router.RouterId[0][0].length);
 
   	  	Log.i("Test"," Loading started bssid list ");
	  	BSSIDList = (String[]) deserialize("bssid.ser");
	  	Log.i("Test"," Data loaded into memory " + BSSIDList.length);
   	  	
   	  	Log.i("Test"," Loading started boolean list ");
        RouterLocation = (boolean[][]) deserialize("routerlocation.ser");
        Log.i("Test"," Deserialized Object " + RouterLocation.length + " " + RouterLocation[0].length);
        
        Log.i("Test"," Loading started map table ");
        map = (int[][]) deserialize("map.ser");
        Log.i("Test"," Deserialized Object " + map.length);
        
        Log.i("Test"," Loading started map table ");
        path = (int[][]) deserialize("path.ser");
        Log.i("Test"," Deserialized Object " + map.length);
        
        Log.i("Test"," Loading started map table ");
        IncidenceMatrix = (int[][]) deserialize("incidencematrix.ser");
        Log.i("Test"," Deserialized Object " + map.length);
        
        Log.i("Test"," Loading started map table ");
        filteredtonearest = (int[]) deserialize("filteredtonearest.ser");
        Log.i("Test"," Deserialized Object " + map.length);
        
        for (int i = 0; i < map.length; i++) {
        	Log.i("Test"," map Object " + map[i][0] + " " + map[i][1] + " " + map[i][2] + " " + i);
		}
        
        
        for(int i = 0; i < IncidenceMatrix.length; i++){
        	String incidence = "";
			for(int j = 0; j < IncidenceMatrix.length; j++){
				incidence += IncidenceMatrix[i][j];
				incidence += ",";
			}
			Log.i("Test","" + incidence);
		}
        
        // Constructing Router-Location association matrix from matrix router (has all readings)
        RouterLocation = new boolean[BSSIDList.length][map.length];
   	  	
   	  	for (int i = 0; i < router.RouterId.length; i++) {     
			for (int j = 0; j < router.RouterId[0].length; j++) {
				for (int j2 = 0; j2 < router.RouterId[0][0].length; j2++) {
					if(router.RouterLevel[i][j][j2]!=0)
						RouterLocation[router.RouterId[i][j][j2]][i] |= true;
				}
			}
	 	}
        
        mgr = getBaseContext().getAssets();
                
    	ResourceProxy resourceProxy = new DefaultResourceProxyImpl(getApplicationContext());
        
        mapView = new BoundedMapView(this, resourceProxy);
        
        double north = 9;
        double east = 27;
        double south = -9;
        double west = -9;
        BoundingBoxE6 bBox = new BoundingBoxE6(north, east, south, west);
        
        mapView.setScrollableAreaLimit(bBox);
        mapView.setUseDataConnection(false);
        mapView.setClickable(true);
        mapView.setBuiltInZoomControls(true);
		mapView.setMultiTouchControls(true);
		mapView.getController().setZoom(5); //set initial zoom-level, depends on your need
		mapView.setTileSource(TileSourceFactory.MAPNIK);
		mapView.setBackgroundColor(0);
		
		RelativeLayout layout = (RelativeLayout) findViewById(R.id.mapLayout);
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT, 284);
		lp.addRule(RelativeLayout.ABOVE, R.id.button1);
		lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
		lp.addRule(RelativeLayout.BELOW, R.id.textView2);
		
		layout.addView(mapView, lp);
		
        t1 = (TextView)findViewById(R.id.textView1);
        t2 = (TextView)findViewById(R.id.textView2);
        
    	//t1.setText("You are near " + locationNames[filteredLocation]);
    	//updateTextViews(filteredLocation+"", "");
    	//t2.setText("Destination : " + locationNames[getNearestLocation(finalLocation)]);

        // Radiogroup of radiobuttons for floor
        floorGroup = (RadioGroup) findViewById(R.id.toggleGroup);
        
        radioBtn0 = (RadioButton) findViewById(R.id.RadioButton0);
        radioBtn1 = (RadioButton) findViewById(R.id.radioButton1);
        
        radioBtn0.setChecked(true);
        
        int checkedRadioButtonID = floorGroup.getCheckedRadioButtonId();
        floorGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
          public void onCheckedChanged(RadioGroup arg0, int id) {
            switch (id) {
            case -1:
            	Log.i("floor"," no floor selected ");
            	break;
            	
            case R.id.RadioButton0:
            	Log.i("floor"," floor 0 selected ");
            	mapView.getController().animateTo(0.0,0.0);
              break;
              
            case R.id.radioButton1:
            	Log.i("floor"," floor 1 selected ");
            	mapView.getController().animateTo(0.0,18.0);
              break;

            default:
              break;
            }
          }
        });
        
        Log.i(" Check Button", " Value " + checkedRadioButtonID);
        
//		/////////////////////////////////////////////////////////////////////////////////
//		mapView = (MapView) findViewById(R.id.mapview);
//		//mapView = new MapView(this, 256);
//		
//		mapView.setUseDataConnection(false); //keeps the mapView from loading online tiles using network connection.
//		
//		mapView.setClickable(true);
//		
//		mapView.setBuiltInZoomControls(true);
//		
//		mapView.setMultiTouchControls(true);
//		
//		mapView.getController().setZoom(5); //set initial zoom-level, depends on your need
//		
//		mapView.setTileSource(TileSourceFactory.MAPNIK);
//		
//		//mapView.getController().setCenter(new GeoPoint((float) lat/1E6,(float) lon/1E6));
//		
//		mapView.setBackgroundColor(0);
		
        // Code for initialising UI for the activity
//        DestinationButton = (Button) findViewById(R.id.button1);
		DestinationButton = (ImageButton) findViewById(R.id.button1);
        
        DestinationButton.setOnClickListener(this);
        
        myLocButton = (ImageButton) findViewById(R.id.button2);
        
        myLocButton.setOnClickListener(this);
    }
    
    @Override
	protected void onStop() {
		super.onStop();
		doEvntUpdt=false;
//		doPoll=false;
		
		// We need an Editor object to make preference changes.
		// All objects are from android.context.Context
		if(myEventsString!=null){
			SharedPreferences settings = getSharedPreferences(PREFS_MYEVENTS, 0);
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("myEventsString", myEventsString);
			editor.commit();
		}
	}
    
    void thread1(){		// poll for event changes
    	new Thread(new Runnable() {
    	    public void run() {
    	            while(doEvntUpdt) {
    	                try {
							Thread.sleep(updtTime);
//							Log.d("hello", ".");
						} catch (InterruptedException e) {
						}
    	                handler.post(new Runnable() {
    	                    public void run() {
    	                    	new UpdateEvents().execute();
    	                    }
    	                });
    	        }}}).start();
    }
    
	@Override
    protected void onResume() {
		if(!Signin.log) {
			Intent i=new Intent(this, MainActivity.class);
			startActivity(i);
		}
		
    	super.onResume();
    	doEvntUpdt=true;
    	doPoll=true;
    	//thread1();
    	
    	
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_NORMAL);
    	
    	wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        
   	  	
   	  	//For locking the Wifi scanner , not to allow continuous scanning for results
   	  	WifiLock wifiLock = wifi.createWifiLock(WifiManager.WIFI_MODE_SCAN_ONLY,"MyLock");
   	  	
   	  	// Lock for wifi
   	  	if(!wifiLock.isHeld()) {
   	  		wifiLock.acquire();
   	  	}
   	  	
   	  	if(!wifi.isWifiEnabled()){
   	  		Toast.makeText(getBaseContext(), "Turn on the Wi-Fi", Toast.LENGTH_SHORT).show();
   	  		startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
   	  	}
   	  	
   	  	// Start Scan 
        onPositionChanged();
        mode = 1;
		wifi.startScan();
    }
	
	@Override
    protected void onPause() {
    	super.onPause();
    	
    	mSensorManager.unregisterListener(this);
    	
    	doEvntUpdt=false;
//    	doPoll=false;
    	
    	//DbHandler.close();
    	
    }
	
	@Override
	public void onBackPressed() {
		doEvntUpdt=false;
//		doPoll=false;
		Intent i=new Intent(this, MainActivity.class);
		startActivity(i);
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	Directions = menu.add("Direction");
    	MyLoc = menu.add("My Location");
    	EventDet = menu.add("Event Details");
    	MyEvents = menu.add("My Events");
    	Setting = menu.add("Settings");
        Logout = menu.add("Sign Out");
        return true;
    }
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		if (item == Search) {
			Intent i=new Intent(this, SearchLoc.class);
			startActivity(i);
			return true;
		} else if (item == MyLoc) {
			onClick(myLocButton);
			return true;
		} else if (item == EventDet) {
			Intent i=new Intent(this, EventDetails.class);
			startActivity(i);
			return true;
		} else if (item == Directions) {
			onClick(DestinationButton);
			return true;
		} else if (item == Setting) {
			Intent i=new Intent(this, Setting.class);
			startActivity(i);
			return true;
		} else if (item == Logout) {
			Signin.log = false;
			
			SharedPreferences settings = getSharedPreferences(Signin.PREFS_NAME, 0);
			SharedPreferences.Editor editor = settings.edit();
			editor.putBoolean("loggedin", Signin.log);
			editor.commit();
			Intent i=new Intent(this, MainActivity.class);
			startActivity(i);
			return true;
		} else if (item == MyEvents) {
			Intent i=new Intent(this, MyEvents.class);
			startActivity(i);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
	
	public Object deserialize(String ObjectName){
    	Object e = null;
    	try {
    		Log.i("Test"," File " + ObjectName);
    		InputStream fileIn = mgr.open(ObjectName);
    		ObjectInputStream in = new ObjectInputStream(fileIn);
    		e = in.readObject();
           	in.close();
           	fileIn.close();
    	} catch(IOException i) {
    		i.printStackTrace();
    		return (Object) e;
    	} catch(ClassNotFoundException c) {
    		c.printStackTrace();
    		return (Object) e;
    	}
		return (Object) e;
    }
    
    public Object deserialize2(String ObjectName){
    	Object e = null;
    	try {
    		FileInputStream fileIn = new FileInputStream(Environment.getExternalStorageDirectory() + "/navigationApp/" +  ObjectName);
    		ObjectInputStream in = new ObjectInputStream(fileIn);
    		e = in.readObject();
           	in.close();
           	fileIn.close();
    	} catch(IOException i) {
    		i.printStackTrace();
    		return (Object) e;
    	} catch(ClassNotFoundException c) {
    		c.printStackTrace();
    		return (Object) e;
    	}
		return (Object) e;
    }

	public MyItemizedIconOverlay addPoint(Drawable drawable, double x, double y){
		
    	MyItemizedIconOverlay mMyOverlay = new MyItemizedIconOverlay(drawable, this, MapActivity.this);
		
		GeoPoint point = new GeoPoint((float) ((lat + y*212963)/1E6), (float) (lon + x*212963)/1E6);
		
		OverlayItem overlayitem;
		
		int nearestlocation = getNearestLocation(filteredLocation);
		if(nearestlocation<1)
			nearestlocation = 1;
		if(drawable==drawable1)			
			overlayitem = new OverlayItem("Current Location", Integer.toString(nearestlocation-1), point);
		else
			overlayitem = new OverlayItem("Destination", Integer.toString(getNearestLocation(finalLocation)), point);
		
		mMyOverlay.addItem(overlayitem);
		return mMyOverlay;
	}

    public void onPositionChanged() {
    	
    	isPositionUpdating = true;
    	
    	// Get the nearest location
    	nearestLocation = filteredtonearest[filteredLocation];
    	
		int[] previous = bfs();
		
		c_y = path[nearestLocation][2];
    	c_x = path[nearestLocation][1];
    	f_y = path[finalLocation][2];
    	f_x = path[finalLocation][1];
    	
    	
    	if(lastFilteredLocation!=filteredLocation){
    		lastFilteredLocation=filteredLocation;
    		new SendLoc().execute();
    	}
    	
		List<Overlay> mapOverlays = mapView.getOverlays();
		
		mapOverlays.clear();
		
		ResourceProxy resourceProxy = new DefaultResourceProxyImpl(getApplicationContext());
        Drawable drawable3 = this.getResources().getDrawable(R.drawable.circlegw);
		
		if(mapView.getZoomLevel()>7) {
			/* adding text overlays */
			MapItemizedOverlay mMyOverlay2 = new MapItemizedOverlay(drawable3, resourceProxy, this, 16);
	
	        for (int i = 0; i < locationXYtoMap.length; i++) {
	        	if(!isLabelled[i]) {
		        	OverlayItem overlayitem2 = new OverlayItem(locationNames[i], Integer.toString(i), 
		        			new GeoPoint(path[locationXYtoMap[i]-1][2]*212963/1E6, path[locationXYtoMap[i]-1][1]*212963/1E6));
		            mMyOverlay2.addOverlay(overlayitem2);
	        	}
			}
	        mapOverlays.add(mMyOverlay2);
	        /* text overlays added */
		}
		
        /* Add tappable overlays */
        MyItemizedIconOverlay mMyOverlay3 = new MyItemizedIconOverlay(drawable3, this, MapActivity.this);

        for (int i = 0; i < locationXYtoMap.length; i++) {
        	if(isLabelled[i] || (!isLabelled[i] && mapView.getZoomLevel()>7)) {
	        	OverlayItem overlayitem2 = new OverlayItem(locationNames[i], Integer.toString(i), 
	        			new GeoPoint(path[locationXYtoMap[i]-1][2]*212963/1E6, path[locationXYtoMap[i]-1][1]*212963/1E6));
	            mMyOverlay3.addItem(overlayitem2);            
        	}
		}
        mapOverlays.add(mMyOverlay3.getOverlay());
        /* tappable overlays added */
		
		
//        MyItemizedIconOverlay mMyOverlay4 = new MyItemizedIconOverlay(drawable1, this, MapActivity.this);
//        
//        OverlayItem overlayitem3 = new OverlayItem("Current Location", Integer.toString(MapActivity.getNearestLocation(filteredLocation + 1)), 
//    			new GeoPoint(c_y*212963/1E6, c_x*212963/1E6));
//        mMyOverlay4.addItem(overlayitem3);
//        mapOverlays.add(mMyOverlay4.getOverlay());
        
        
        MyItemizedIconOverlay mMyOverlay5 = new MyItemizedIconOverlay(drawable2, this, MapActivity.this);
        
        OverlayItem overlayitem4 = new OverlayItem("Destination", Integer.toString(MapActivity.getNearestLocation(finalLocation + 1)), 
    			new GeoPoint(f_y*212963/1E6, f_x*212963/1E6));
        mMyOverlay5.addItem(overlayitem4);
		mapOverlays.add(mMyOverlay5.getOverlay());
		
        mapView.postInvalidate();
        
        ////  Path of navigation ////
        PathOverlay myPath = new PathOverlay(Color.RED, this);
        
        //myPath.getPaint().setStrokeWidth(mapView.getZoomLevel()*4);
        Log.d("Style", "" + myPath.getPaint().getStyle());
        myPath.getPaint().setStrokeWidth(10.0f);
        
        myPath.addPoint(new GeoPoint(path[finalLocation][2]*212963/1E6, path[finalLocation][1]*212963/1E6));
        
        int current = finalLocation;
        
        Log.d("PATH", "final location" + finalLocation);
        
        if (current != nearestLocation){
	        while(previous[current] != nearestLocation){
	        	if (current == nearestLocation){
	        		break;
	        	}
	        	
	        	// Check if current and next location is on different floors
	        	for (int i = 0; i < floorchange.length; i++) {
					if((current == floorchange[i][0]-1 && previous[current] == floorchange[i][1]-1)||(current == floorchange[i][1]-1 && previous[current] == floorchange[i][0]-1)){
						// Break the path and start a new path
						mapOverlays.add(myPath);
						myPath = new PathOverlay(Color.RED, this);
						myPath.getPaint().setStrokeWidth(10.0f);
						break;
					}
				}
	        	
	        	myPath.addPoint(new GeoPoint(path[previous[current]][2]*212963/1E6, path[previous[current]][1]*212963/1E6));
	        	current = previous[current];
	        	Log.d("PATH", "Previous Location:" + current);
	        }
	    }
        
        boolean isFloorChange = true;
        // Check if current and next location is on different floors
    	for (int i = 0; i < floorchange.length; i++) {
			if((current == floorchange[i][0]-1 && previous[current] == floorchange[i][1]-1)||(current == floorchange[i][1]-1 && previous[current] == floorchange[i][0]-1)){
				isFloorChange = false;
			}
		}
    	
		if(isFloorChange) {
			myPath.addPoint(new GeoPoint(path[nearestLocation][2]*212963/1E6, path[nearestLocation][1]*212963/1E6));
		} 
		
		Log.d("PATH", "intial location" + nearestLocation);
		mapOverlays.add(myPath);
		
		// Add current position marker
		addCurrentPositionMarker();
		
		isPositionUpdating = false;
        
        mapView.postInvalidate();
        //t2.setText("Destination : " + locationNames[getNearestLocation(finalLocation)]);
//        int nearestLocation = getNearestLocation(finalLocation);
//    	Log.d("Update", "Current Location " + finalLocation + " Nearest Location " + nearestLocation);
//    	if(nearestLocation>=1)
//    		this.t2.setText("Destination : " + locationNames[nearestLocation-1]);
        updateTextViews(nearestLocation+"", "");
    }
    
    public BitmapDrawable rotateDrawable(float angle)
    {
      Bitmap arrowBitmap = BitmapFactory.decodeResource(getBaseContext().getResources(), 
                                                        R.drawable.arrow48);
      // Create blank bitmap of equal size
      Bitmap canvasBitmap = BitmapFactory.decodeResource(getBaseContext().getResources(), 
              R.drawable.blank48);
      
      Bitmap canvasBitmap_mutable = canvasBitmap.copy(Bitmap.Config.ARGB_8888, true);
      
      // Create canvas
      Canvas canvas = new Canvas(canvasBitmap_mutable);
      
      // Create rotation matrix
      Matrix rotateMatrix = new Matrix();
      rotateMatrix.setRotate(angle, canvas.getWidth()/2, canvas.getHeight()/2);
      
      // Draw bitmap onto canvas using matrix
      canvas.drawBitmap(arrowBitmap, rotateMatrix, null);

      return new BitmapDrawable(canvasBitmap_mutable); 
    }
    
    private void updateTextViews(String loc, String que) {    	
    	int nearestLocation = getNearestLocation(Integer.parseInt(loc)+1);
    	//Log.d("Update", "Current Location " + loc + " Nearest Location " + nearestLocation);
    	if(nearestLocation>=0)
    		this.t1.setText("You are near " + locationNames[nearestLocation]);
    	
    	nearestLocation = getNearestLocation(finalLocation+1);
    	Log.d("Update", "Destination " + finalLocation + " Nearest Location " + nearestLocation);
    	if(nearestLocation>=0)
    		this.t2.setText("Destination : " + locationNames[nearestLocation]);
		
    }
    
    public static int getNearestLocation(int loc) {
    	for (int i = 0; i < locationXYtoMap.length; i++) {
			if(locationXYtoMap[i]==loc)
				return i;
		}
    	return -1;
//    	int nearestLocation = 0;
//    	double minDistance = getDistance(0, loc);
//    	for (int i = 1; i < locationXY.length; i++) {
//    		double distance = getDistance(i, loc);
//			if(minDistance > distance) {
//				minDistance = distance;
//				nearestLocation = i;
//			}
//		}
//    	return nearestLocation;
    }
    
    public int[] bfs(){
    	int no_of_nodes = IncidenceMatrix.length;
    	// This algorithm is used for updating distances of nodes
    	double[] dist = new double[no_of_nodes];
    	// At every index of this array, we have the index of previous element
    	int[] previous = new int[no_of_nodes];
    	// This is a list
    	List<Integer> Q = new ArrayList<Integer>();
    	
    	// Initialize the arrays
    	for(int i = 0; i < no_of_nodes; i++){
    		dist[i] = Double.MAX_VALUE;    		
    		previous[i] = 0;
    		// Add all elements in the list
    		Q.add(i);
    	}
    	
    	// Distance of current element is 0
    	dist[nearestLocation] = 0;
    	//dist[MainActivity.prevloc] = 0;
    	//Log.d("Path", "Current Location:" + MainActivity.location + " Final Location: " + MainActivity.finalloc);
    	
    	while(!Q.isEmpty()){
    		double min = Double.MAX_VALUE;
    		Integer minnode = 0;
    		int node;
    		ListIterator<Integer> it = Q.listIterator();
    		while(it.hasNext()){
    			node = it.next();
    			//Log.d("Path", "Iterator for min: " + node);
    			if (dist[node] <= min){
    				min = dist[node];
    				minnode = node;
    				//Log.d("Path", "Choosing minnode: " + minnode);
    			}
    		}
    		Q.remove(minnode);
    		
    		//Log.d("Path", "Previous Location: " + Q.toString());
    		if(dist[minnode] == Double.MAX_VALUE){
    			//Log.d("Path", "Inside if " + dist[minnode]);
    			break;
    		}
    		
    		for(int i = 0; i < no_of_nodes; i++){
    			if(IncidenceMatrix[minnode][i] != 0){
    				//Log.d("Path", "Inside if " + i);
    				//double temp = dist[minnode] + 1;
    				double temp = dist[minnode] + IncidenceMatrix[minnode][i];
    				if(temp < dist[i]){
    					dist[i] = temp;
    					//Log.d("Path", "Inside if " + dist[i]);
    					previous[i] = minnode;
    				}
    			}
    		}
    		if (minnode == finalLocation){
    			for (int i = 0; i < dist.length; i++) {
    				Log.d("Dist Array", "" + dist[i]);
				}
    			return previous;
    		}
    	}
    	return previous;
    }
    
    public static int getIndexOfBSSID(String bssid) {
    	for(int i = 0; i < BSSIDList.length; i++) {
    		if(BSSIDList[i].equals(bssid))
    			return i;
    	}
    	return -1;
    }

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.button1) {
			Intent i=new Intent(this, SearchLoc.class);
			startActivity(i);
			onPositionChanged();
		}
		
		if (v.getId() == R.id.button2) {			
			if (map[filteredLocation][1] > 42){
				radioBtn0.setChecked(false);
				radioBtn1.setChecked(true);
			} else {
				radioBtn0.setChecked(true);
				radioBtn1.setChecked(false);
			}
			mapView.getController().animateTo((float) ((lat + (map[filteredLocation][2])*212963)/1E6), (float) (lon + (map[filteredLocation][1])*212963)/1E6);
		}
	}
	
	public void getEventObj() {
		SharedPreferences settings = getSharedPreferences(PREFS_EVENTS_DATA, 0);
		String eventsdata=settings.getString("eventsdata", null);
//		Log.d("hello","evdata=="+eventsdata);
		if(eventsdata==null){
			new UpdateEvents().execute();
			return;
		}
		try{
			String jsonstring=eventsdata;
			JSONArray jArray = new JSONArray(jsonstring);
			JSONObject jsonObject;
			int i=0;
			jsonObject = new JSONObject(jArray.getString(i));
			while (jsonObject != null) {
				if(e==null)
					e=new ArrayList<Event>();

				int s = e.size();
				
				String a = jsonObject.getString("EventID");
				int newID=Integer.parseInt(a);
				int fl=0;
				
				for(int j=0;j<s;j++){
					if(e.get(j).EventID==newID){
						s=j;
						fl=1;
						break;
					}
				}
				
				if(fl==0){
					e.add(new Event());
					s = e.size()-1;
				}
				
				e.get(s).EventID=newID;
//				Log.d("hello", "entry-"+e.get(s).EventID+"");
				
				a = jsonObject.getString("Name");
				e.get(s).Name=a;
//				Log.d("hello", "entry-"+e.get(s).Name);
				
				a = jsonObject.getString("Description");
				e.get(s).Description=a;
//				Log.d("hello", "entry-"+e.get(s).Description);
				
				a = jsonObject.getString("LocationID");
				e.get(s).LocationID=Integer.parseInt(a);
//				Log.d("hello", "entry-"+e.get(s).LocationID+"");
				
				a = jsonObject.getString("StartTime");
				e.get(s).StartTime=a;
//				Log.d("hello", "entry-"+e.get(s).StartTime+"");
				
				a = jsonObject.getString("Duration");
				e.get(s).Duration=Integer.parseInt(a);
//				Log.d("hello", "entry-"+e.get(s).Duration);
				
				i++;
				jsonObject = new JSONObject(jArray.getString(i));
			}
		} catch(JSONException e) {
			Log.d("hello","json ex");
		}
	}
	
	class SendLoc extends AsyncTask<Integer, String, Integer> {
		protected Integer doInBackground(Integer... arg) {
			sendlocation();
			return 0;
		}

		protected void onPostExecute(Integer arg) {
			if (sendLoc) {
				publishProgress("Loc sending Successful");
			} else {
				publishProgress("Loc sending Failed");
			}
		}
		
		private void sendlocation() {
			try {
				sendLoc=false;
				publishProgress("Connecting...");
				URL url = new URL("https://www.sentimeter.me/imap/store_userlocation.php");
				HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
				String charset = "UTF-8";
				urlConnection.setRequestMethod("POST");
				urlConnection.setDoOutput(true); // Triggers POST.
				urlConnection.setDoInput(true);
				urlConnection.setRequestProperty("Accept-Charset", charset);
				urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);
				OutputStream output = null;

				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
				
				String UId=settings.getString("UserID", null);
				
				Log.d("hello","Uid from SPref-"+UId);
				
				String query = "UserID="+UId+"&LocationID="+lastFilteredLocation;
				
				Log.d("hello","loc qry:"+query);
				try {
				     output = urlConnection.getOutputStream();
				     output.write(query.getBytes(charset));
				} finally {
				     if (output != null) try { output.close(); } catch (IOException logOrIgnore) {}
				}
				
				urlConnection.setReadTimeout(5000);
				
				urlConnection.connect();

				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

				String responseBody = bufferedReader.readLine();
				
				if(responseBody==null)
					Log.d("hello","loc null resp");
				else if (responseBody.indexOf("false")!=-1){
					Log.d("hello","loc resp-"+responseBody);
					sendLoc=false;
				} else if (responseBody.indexOf("true")!=-1){
					sendLoc=true;
				}
				
			} catch (MalformedURLException e) {
				publishProgress("Url");
			} catch (IOException e) {
				publishProgress("Timeout");
			} catch (NullPointerException e) {
				publishProgress("Null");
			}
		}

		protected void onProgressUpdate(String... values) {
				Log.d("hello", values[0]);
//				Toast.makeText(getBaseContext(), values[0], Toast.LENGTH_SHORT).show();
		}
	}
	
	class SplMsgPoll extends AsyncTask<String, String, Integer> {
		int MId=0;
		protected Integer doInBackground(String... arg) {
			while(doPoll){
				poll();
//				Log.d("hello",".");
				try {
					Thread.sleep(pollTime);
				} catch (InterruptedException e) {
				}
			}
			return 0;
		}

		protected void onPostExecute(Integer arg) {
			Log.d("hello","Poll --- "+pollStatus);
			if (pollStatus) {
				publishProgress("Poll Successful");
				
				
			} else {
				publishProgress("Poll Failed");
			}
		}
		
		private void poll() {
			try {
				pollStatus=false;
				publishProgress("Connecting...");
				URL url = new URL("https://www.sentimeter.me/imap/later_message.php");
				HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
				String charset = "UTF-8";
				urlConnection.setRequestMethod("POST");
				urlConnection.setDoOutput(true); // Triggers POST.
				urlConnection.setDoInput(true);
				urlConnection.setRequestProperty("Accept-Charset", charset);
				urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);
				OutputStream output = null;

				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
				
				MId=settings.getInt("MessageID", 0);
				
				Log.d("hello","MsgId from SPref-"+MId);
				
				String query = "MessageID="+MId;
				
				Log.d("hello","qry:"+query);
				try {
				     output = urlConnection.getOutputStream();
				     output.write(query.getBytes(charset));
				} finally {
				     if (output != null) try { output.close(); } catch (IOException logOrIgnore) {}
				}
				
				urlConnection.setReadTimeout(5000);
				
				urlConnection.connect();

				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

				String responseBody = bufferedReader.readLine();
				
				if(responseBody==null) {
					Log.d("hello","null resp");
					pollStatus=false;
					return;
				}
				else if (responseBody.indexOf("Information Insufficient")!=-1||responseBody.indexOf("[]")!=-1){
					Log.d("hello","poll resp-"+responseBody);
					pollStatus=false;
					return;
				} else {
//					Log.d("hello","poll resp-"+responseBody);
					pollStatus=true;
				}
				int newID=MId;
				try{
					String jsonstring=responseBody;
					JSONArray jArray = new JSONArray(jsonstring);
					JSONObject jsonObject;
					int i=0;
					jsonObject = new JSONObject(jArray.getString(i));
					while (jsonObject != null) {
						String ntitle = jsonObject.getString("Title");
						String ncontent = jsonObject.getString("Content");
						String a = jsonObject.getString("MessageID");
						newID=Integer.parseInt(a);
						
						NotificationCompat.Builder mBuilder =
						        new NotificationCompat.Builder(MapActivity.this)
						        .setSmallIcon(R.drawable.ic_launcher)
						        .setContentTitle(ntitle)
						        .setContentText(ncontent);
						// Creates an explicit intent for an Activity in your app
						Intent resultIntent = new Intent(MapActivity.this, MapActivity.class);

						// The stack builder object will contain an artificial back stack for the
						// started Activity.
						// This ensures that navigating backward from the Activity leads out of
						// your application to the Home screen.
						TaskStackBuilder stackBuilder = TaskStackBuilder.create(MapActivity.this);
						// Adds the back stack for the Intent (but not the Intent itself)
						stackBuilder.addParentStack(MapActivity.class);
						// Adds the Intent that starts the Activity to the top of the stack
						stackBuilder.addNextIntent(resultIntent);
						PendingIntent resultPendingIntent =
						        stackBuilder.getPendingIntent(
						            0,
						            PendingIntent.FLAG_UPDATE_CURRENT
						        );
						mBuilder.setContentIntent(resultPendingIntent);
						NotificationManager mNotificationManager =
						    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
						// mId allows you to update the notification later on.
						mNotificationManager.notify(i, mBuilder.build());
						
						i++;
						jsonObject = new JSONObject(jArray.getString(i));
						
					}
					
				} catch(JSONException e) {
					Log.d("hello","json ex");
					MId=newID;
					Log.d("hello","new mid "+MId);
					pollStatus=true;
					settings = getSharedPreferences(PREFS_NAME, 0);
					SharedPreferences.Editor editor = settings.edit();
					editor.putInt("MessageID", MId);
					editor.commit();
				}
				
			} catch (MalformedURLException e) {
				publishProgress("Url");
			} catch (IOException e) {
				publishProgress("Timeout");
			} catch (NullPointerException e) {
				publishProgress("Null");
			}
		}

		protected void onProgressUpdate(String... values) {
				Log.d("hello", values[0]);
//				Toast.makeText(getBaseContext(), values[0], Toast.LENGTH_SHORT).show();
		}
	}	
	
	class UpdateEvents extends AsyncTask<String, String, Integer> {

		protected Integer doInBackground(String... arg) {
			update();
			return 0;
		}

		protected void onPostExecute(Integer arg) {
			if (auth) {
				publishProgress("Event Update Successful");
			} else {
				publishProgress("Event Update Failed");
			}
		}
		
		private void update() {
			try {
				auth=false;
				publishProgress("Connecting...");
				URL url = new URL("https://www.sentimeter.me/imap/aftertime_event.php");
				HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
				String charset = "UTF-8";
				urlConnection.setRequestMethod("POST");
				urlConnection.setDoOutput(true); // Triggers POST.
				urlConnection.setDoInput(true);
				urlConnection.setRequestProperty("Accept-Charset", charset);
				urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);
				OutputStream output = null;

				SharedPreferences settings = getSharedPreferences(PREFS_EVENT_TIMESTAMP, 0);
				TIME_STAMP=settings.getString("timestamp", null);
				
				String newTimeStamp=null;
				
				Log.d("hello","TS from SP-"+TIME_STAMP);
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
				newTimeStamp = sdf.format(new Date());
				if(TIME_STAMP==null) {
					TIME_STAMP = sdf.format(new Date(0));
				}
				Log.d("hello","last update-"+TIME_STAMP);
				
				//TIME_STAMP="2012-12-18 21:56:36";
				
				String query = "TimeStamp="+TIME_STAMP;
				
				Log.d("hello","qry:"+query);
				try {
				     output = urlConnection.getOutputStream();
				     output.write(query.getBytes(charset));
				} finally {
				     if (output != null) try { output.close(); } catch (IOException logOrIgnore) {}
				}
				
				urlConnection.setReadTimeout(5000);
				
				urlConnection.connect();

				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

				String responseBody = bufferedReader.readLine();
				
				if(responseBody==null)
					Log.d("hello","null resp");
				else {
//					Log.d("hello","resp-"+responseBody);
				}
					
				try{
					String jsonstring=responseBody;
					JSONArray jArray = new JSONArray(jsonstring);
					JSONObject jsonObject;
					int i=0;
					jsonObject = new JSONObject(jArray.getString(i));
					while (jsonObject != null) {
						if(e==null)
							e=new ArrayList<Event>();

						int s = e.size();
						
						String a = jsonObject.getString("EventID");
						int newID=Integer.parseInt(a);
						int fl=0;
						
						for(int j=0;j<s;j++){
							if(e.get(j).EventID==newID){
								s=j;
								fl=1;
								break;
							}
						}
						
						if(fl==0){
							e.add(new Event());
							s = e.size()-1;
						}
						
						e.get(s).EventID=newID;
//						Log.d("hello", "entry-"+e.get(s).EventID+"");
						
						a = jsonObject.getString("Name");
						e.get(s).Name=a;
//						Log.d("hello", "entry-"+e.get(s).Name);
						
						a = jsonObject.getString("Description");
						e.get(s).Description=a;
//						Log.d("hello", "entry-"+e.get(s).Description);
						
						a = jsonObject.getString("LocationID");
						e.get(s).LocationID=Integer.parseInt(a);
//						Log.d("hello", "entry-"+e.get(s).LocationID+"");
						
						a = jsonObject.getString("StartTime");
						e.get(s).StartTime=a;
//						Log.d("hello", "entry-"+e.get(s).StartTime+"");
						
						a = jsonObject.getString("Duration");
						e.get(s).Duration=Integer.parseInt(a);
//						Log.d("hello", "entry-"+e.get(s).Duration);
						
						i++;
						jsonObject = new JSONObject(jArray.getString(i));
					}
				} catch(JSONException e) {
					Log.d("hello","json ex");
				}
				
				if((responseBody.indexOf("Information Improper")==-1
						&&responseBody.indexOf("<HTML><HEAD>")==-1)||responseBody.indexOf("[]")==-1){
					auth = true;
					Log.d("hello","Updated at-"+newTimeStamp);
					settings = getSharedPreferences(PREFS_EVENT_TIMESTAMP, 0);
					SharedPreferences.Editor editor = settings.edit();
					editor.putString("timestamp", newTimeStamp);
					editor.commit();
					
					String newEventsData = null;
					try{
						JSONArray jArray = new JSONArray();
						JSONObject jsonObject;
						int i=0;
						
						while (i<e.size()) {
							
							jsonObject = new JSONObject();
							String a = e.get(i).EventID+"";
							jsonObject.put("EventID", a );
							
							a = e.get(i).Name;
							jsonObject.put("Name", a );
							
							a = e.get(i).Description;
							jsonObject.put("Description", a );
							
							a = e.get(i).LocationID+"";
							jsonObject.put("LocationID", a );
							
							a = e.get(i).StartTime;
							jsonObject.put("StartTime", a );

							a = e.get(i).Duration+"";
							jsonObject.put("Duration", a );
							
							i++;
							jArray.put(jsonObject);
						}
						newEventsData=jArray.toString(); 
					} catch(JSONException e) {
						Log.d("hello","json ex");
					}
					
					settings = getSharedPreferences(PREFS_EVENTS_DATA, 0);
					editor = settings.edit();
					editor.putString("eventsdata", newEventsData);
					editor.commit();
					
					Log.d("hello","done");
					
				}
			} catch (MalformedURLException e) {
			} catch (IOException e) {
				publishProgress("Timeout");
			} catch (NullPointerException e) {
			}
		}

		protected void onProgressUpdate(String... values) {
			Log.d("hello", values[0]);
			Toast.makeText(getApplicationContext(), values[0], Toast.LENGTH_SHORT).show();
		}
	}
	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		synchronized (this) {

			if (event.sensor == mAccelerometer) {
	            System.arraycopy(event.values, 0, mLastAccelerometer, 0, event.values.length);
	            mLastAccelerometerSet = true;
	        } else if (event.sensor == mMagnetometer) {
	            System.arraycopy(event.values, 0, mLastMagnetometer, 0, event.values.length);
	            mLastMagnetometerSet = true;
	        }
	        
			if (mLastAccelerometerSet && mLastMagnetometerSet) {
	            SensorManager.getRotationMatrix(mR, null, mLastAccelerometer, mLastMagnetometer);
	            SensorManager.getOrientation(mR, mOrientation);
	            
				if (mOrientation[0] > 0){
	            	azimuth = mOrientation[0]*rad2deg;
	            } else {
	            	azimuth = mOrientation[0]*rad2deg + 360;
	            }
				
				azimuth = (azimuth + 155)%360;
				addCurrentMarker();
	        }
			
		}
	}
	
	public void addCurrentMarker(){
		if(mapView.getOverlays().size() == 0){
			addCurrentPositionMarker();
	        mapView.postInvalidate();
		} else if (!isPositionUpdating) {
			List<Overlay> mapOverlays = mapView.getOverlays();
			// It is ensured that last marker will be current position marker
			mapOverlays.remove(mapOverlays.size() - 1);
			
			addCurrentPositionMarker();
	        mapView.postInvalidate();
        }
	}
	
	public void addCurrentPositionMarker(){
		
		if ((mAccelerometer != null) && (mMagnetometer != null)){
			drawable1 = rotateDrawable(azimuth);
		} else {
			drawable1 = MapActivity.this.getResources().getDrawable(R.drawable.person_small);
		}
		List<Overlay> mapOverlays = mapView.getOverlays();
		mMyOverlay4 = new MyItemizedIconOverlay(drawable1, this, MapActivity.this);
		OverlayItem overlayitem3 = new OverlayItem("Current Location", Integer.toString(MapActivity.getNearestLocation(filteredLocation + 1)), 
    			new GeoPoint(c_y*212963/1E6, c_x*212963/1E6));
        mMyOverlay4.addItem(overlayitem3);
        mapOverlays.add(mMyOverlay4.getOverlay());
	}

}