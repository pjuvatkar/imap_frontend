package com.uiactivities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;

public class Misc {
	
	public static void copyFile(String outFilePath, String inFilePath) throws IOException{
		
		//Open your local db as the input stream
		if(new File(inFilePath).exists()) {
			InputStream myInput = new FileInputStream(inFilePath);
			
	    	//Open the empty db as the output stream
	    	OutputStream myOutput = new FileOutputStream(outFilePath);
	    	
	    	//transfer bytes from the inputfile to the outputfile
	    	byte[] buffer = new byte[1024];
	    	int length;
	    	while ((length = myInput.read(buffer))>0){
	    		myOutput.write(buffer, 0, length);
	    	}
	    	
	    	//Close the streams
	    	myOutput.flush();
	    	myOutput.close();
	    	myInput.close();
		} else {
			Log.d("Database","Input file not found");
		}
    }
	
	//Method to copy map file from assets to sd card
	public static void copyMap(AssetManager mgr, String ObjectName) {
		try {
			//Get the path of the sdcard
			File sd = Environment.getExternalStorageDirectory();
			
			String outFilePath = Environment.getExternalStorageDirectory().toString()+"/osmdroid";

			//If the write permission for sd card is given
			if (sd.canWrite()) {
                //The path to which the database needs to be saved
                File file = new File(Environment.getExternalStorageDirectory().toString()+"/osmdroid");
                
                // Remove the previous map from sd card
                deleteDirectory(file);                
                
                //If the path do not exit, create the necessary folders
                if (!file.exists()) {
                	File folder = new File(Environment.getExternalStorageDirectory().toString()+"/osmdroid");
	                folder.mkdirs();
            	}
                
                file = new File(Environment.getExternalStorageDirectory().toString()+"/osmdroid"+"/"+ObjectName);
                
                if (!file.exists()) {
                	//Open your local db as the input stream
        			InputStream myInput = mgr.open(ObjectName);
        			
        	    	//Open the empty db as the output stream
        	    	OutputStream myOutput = new FileOutputStream(outFilePath + "/" +  ObjectName);
        	    	
        	    	//transfer bytes from the inputfile to the outputfile
        	    	byte[] buffer = new byte[1024];
        	    	int length;
        	    	while ((length = myInput.read(buffer))>0){
        	    		myOutput.write(buffer, 0, length);
        	    	}
        	    	
        	    	//Close the streams
        	    	myOutput.flush();
        	    	myOutput.close();
        	    	myInput.close();
        	    	Log.i("Test","Map Copied");
            	}
    		}
		}
		catch (Exception e) {
			Log.w("Settings Backup", e);
		}
	}
	
	public static boolean deleteDirectory(File path) {
	    // TODO Auto-generated method stub
	    if( path.exists() ) {
	        File[] files = path.listFiles();
	        for(int i=0; i<files.length; i++) {
	        	if(files[i].isDirectory()) {
	                deleteDirectory(files[i]);
	            }
	            else {
	                files[i].delete();
	            }
	        }
	    }
	    return(path.delete());
	}
}