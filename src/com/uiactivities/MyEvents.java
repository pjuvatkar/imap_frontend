package com.uiactivities;

import static com.uiactivities.MapActivity.e;
import static com.uiactivities.MapActivity.locationNames;
import static com.uiactivities.MapActivity.myEvents;
import static com.uiactivities.MapActivity.myEventsString;
import static com.uiactivities.Signin.PREFS_NAME;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MyEvents extends Activity {
	static final String PREFS_MYEVENTS = "myEventsPrefsFile";
	String desc = "Desc ";
	public String[] newname;
	public int[] newloc, loc1;
	public int[] newduration, duration1;
	public static String[] newstarttime;
	public int position;
	public boolean regfetchStatus=false, unregStatus=false;
	
	ArrayAdapter<String> adapter ;
	List<Map<String, String>> eventslist;
	private MenuItem Clear;
	private MenuItem Update;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_eventdetails);
		loadMyEvents();
		SharedPreferences settings = getSharedPreferences(PREFS_MYEVENTS, 0);
		myEventsString=settings.getString("myEventsString", null);
		
		if(myEventsString==null){
			Log.d("hello1","!"+myEventsString);
			ListView listview = (ListView) findViewById(R.id.listView1);
			List<String> litems = new ArrayList<String>();
			litems.add("Please update to fetch your registered events.");
			litems.add("Open menu from dedicated menu button or action bar. Update.");
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_list_item_1, litems);
			listview.setAdapter(adapter);
//			new FetchRegEvents().execute();
			return;
		}
		
		List<String> sortbylist = new ArrayList<String>();
		sortbylist.add("Name");
		sortbylist.add("Distance");
		sortbylist.add("Time"); // or whatever required
		
		newname=new String[myEvents.length];
		newloc=new int[myEvents.length];
		newduration=new int[myEvents.length];
		newstarttime=new String[myEvents.length];
		
		loc1=new int[myEvents.length];
		duration1=new int[myEvents.length];
		
		ArrayAdapter<String> sortspinneradapter = new ArrayAdapter<String>(
				this, android.R.layout.simple_spinner_item, sortbylist);
		sortspinneradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Spinner s = (Spinner) findViewById(R.id.spinner1);
		s.setAdapter(sortspinneradapter);
		s.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				String item = ((TextView) arg1).getText().toString();
				Toast.makeText(getBaseContext(), item, Toast.LENGTH_SHORT).show();
	
				if(item.equals("Name")){
					newname=myEvents.clone();
					Arrays.sort(newname);
					for (int i = 0; i < newname.length; i++) {
						for (int j = 0; j < e.size(); j++) {
							if(newname[i].equals(e.get(j).Name)){
								newloc[i]=e.get(j).LocationID;
								newduration[i]=e.get(j).Duration;
								newstarttime[i]=e.get(j).StartTime;
							}
						}
					}
					getEventsList();
				} else if(item.equals("Distance")){
					newname=myEvents.clone();
					for (int i = 0; i < newname.length; i++) {
						for (int j = 0; j < e.size(); j++) {
							if(newname[i].equals(e.get(j).Name)){
								loc1[i]=e.get(j).LocationID;
								duration1[i]=e.get(j).Duration;
							}
						}
					}
					
					int[] dist=new int[myEvents.length];
					for (int i = 0; i < myEvents.length; i++) {
						dist[i]=getCurrentDistance(loc1[i]);
					}
					int[] newdist;
					newdist=dist.clone();
					Arrays.sort(newdist);
					List<Integer> j1=new ArrayList<Integer>();
					for (int i = 0; i < newdist.length; i++) {
						for (int j = 0; j < dist.length; j++) {
							if(!j1.contains(j)&&newdist[i]==dist[j]){
								newname[i]=myEvents[j];
								newloc[i]=loc1[j];
								newduration[i]=duration1[j];
								newstarttime[i]=e.get(j).StartTime;
								i++;
								if(i==newdist.length)
									break;
								j1.add(j);
								j=0;
							}
						}
					}
					
					getEventsList();
					
				} else if(item.equals("Time")){
					newname=myEvents.clone();
					for (int i = 0; i < newname.length; i++) {
						for (int j = 0; j < e.size(); j++) {
							if(newname[i].equals(e.get(j).Name)){
								loc1[i]=e.get(j).LocationID;
								duration1[i]=e.get(j).Duration;
							}
						}
					}
					int[] time=new int[myEvents.length];
					for (int i = 0; i < myEvents.length; i++) {
//						time[i]=Integer.parseInt(duration1[i].split(":")[0]);
						String x=e.get(i).StartTime;
						time[i]=Integer.parseInt(x.substring(x.indexOf(" ")+1,x.indexOf(":")));
//						Log.d("hello",time[i]+" time");
					}
					int[] newtime;
					newtime=time.clone();
					Arrays.sort(newtime);
					List<Integer> j1=new ArrayList<Integer>();
					for (int i = 0; i < newtime.length; i++) {
						for (int j = 0; j < time.length; j++) {
							if(!j1.contains(j)&&newtime[i]==time[j]){
								newname[i]=myEvents[j];
								newloc[i]=loc1[j];
								newduration[i]=duration1[j];
								newstarttime[i]=e.get(j).StartTime;
								i++;
								if(i==newtime.length)
									break;
								j1.add(j);
								j=0;
							}
						}
					}
					
					getEventsList();
				}
			}

			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Clear = menu.add("Clear");
		Update = menu.add("Update");
        return true;
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item == Clear) {
			myEventsString=null;
			SharedPreferences settings = getSharedPreferences(PREFS_MYEVENTS, 0);
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("myEventsString", myEventsString);
			editor.commit();
			Intent i=new Intent(MyEvents.this, MyEvents.class);
			startActivity(i);
			finish();
			return true;
		} if (item == Update){
			new FetchRegEvents().execute();
		} else
			Log.d("hello", ":D");
		return true;
	}
	
	class FetchRegEvents extends AsyncTask<String, String, Integer> {
		String newMyEvents;
		protected Integer doInBackground(String... arg) {
			fetchreg();
			return 0;
		}

		protected void onPostExecute(Integer arg) {
			if (regfetchStatus) {
				publishProgress("Reg ev fetch Successful");
				SharedPreferences settings = getSharedPreferences(PREFS_MYEVENTS, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putString("myEventsString", newMyEvents);
				editor.commit();
//				Log.d("hello",newMyEvents);
				Intent i=new Intent(MyEvents.this, MyEvents.class);
				startActivity(i);
				finish();
			} else {
				publishProgress("Reg ev fetch Failed");
			}
		}
		
		private void fetchreg() {
			try {
				regfetchStatus=false;
				publishProgress("Connecting...");
				URL url = new URL("https://www.sentimeter.me/imap/registered_event.php");
				HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
				String charset = "UTF-8";
				urlConnection.setRequestMethod("POST");
				urlConnection.setDoOutput(true); // Triggers POST.
				urlConnection.setDoInput(true);
				urlConnection.setRequestProperty("Accept-Charset", charset);
				urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);
				OutputStream output = null;

				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
				
				String UId=settings.getString("UserID", null);
				
				Log.d("hello","Uid from SPref-"+UId);
				
				String query = "UserID="+UId;
				
				Log.d("hello","qry:"+query);
				try {
				     output = urlConnection.getOutputStream();
				     output.write(query.getBytes(charset));
				} finally {
				     if (output != null) try { output.close(); } catch (IOException logOrIgnore) {}
				}
				
				urlConnection.setReadTimeout(5000);
				
				urlConnection.connect();

				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

				String responseBody = bufferedReader.readLine();
				
				if(responseBody==null){
					Log.d("hello","null resp");
					regfetchStatus=false;
				}
				else if(responseBody.indexOf("[]")!=-1){
					regfetchStatus=true;
				}
				else {
//					Log.d("hello","resp-"+responseBody);
					regfetchStatus=true;
				}
					
				try{
					String jsonstring=responseBody;
					JSONArray jArray = new JSONArray(jsonstring);
					JSONObject jsonObject;
					int i=0;
					jsonObject = new JSONObject(jArray.getString(i));
					newMyEvents=new String();
					while (jsonObject != null) {

						int s = e.size();
						
						String a = jsonObject.getString("EventID");
						int newID=Integer.parseInt(a);
						
						for(int j=0;j<s;j++){
							if(e.get(j).EventID==newID){
								newMyEvents+=e.get(j).Name+";";
								break;
							}
						}
						
						i++;
						jsonObject = new JSONObject(jArray.getString(i));
						
					}
					
				} catch(JSONException e) {
					Log.d("hello","json ex");
				}
				
			} catch (MalformedURLException e) {
				publishProgress("Url");
			} catch (IOException e) {
				publishProgress("Timeout");
			} catch (NullPointerException e) {
				publishProgress("Null");
			}
		}

		protected void onProgressUpdate(String... values) {
				Log.d("hello", values[0]);
				Toast.makeText(getBaseContext(), values[0], Toast.LENGTH_SHORT).show();
		}
	}
	
	class UnregEvent extends AsyncTask<Integer, String, Integer> {
		protected Integer doInBackground(Integer... arg) {
			unreg(arg);
			return 0;
		}

		protected void onPostExecute(Integer arg) {
			if (unregStatus) {
				publishProgress("Unreg Successful");
				new FetchRegEvents().execute();
			} else {
				publishProgress("Unreg Failed");
			}
		}
		
		private void unreg(Integer[] arg) {
			try {
				unregStatus=false;
				publishProgress("Connecting...");
				URL url = new URL("https://www.sentimeter.me/imap/delete_userevent.php");
				HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
				String charset = "UTF-8";
				urlConnection.setRequestMethod("POST");
				urlConnection.setDoOutput(true); // Triggers POST.
				urlConnection.setDoInput(true);
				urlConnection.setRequestProperty("Accept-Charset", charset);
				urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);
				OutputStream output = null;

				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
				
				String UId=settings.getString("UserID", null);
				
				Log.d("hello","Uid from SPref-"+UId);
				
				String query = "UserID="+UId+"&EventID="+arg[0];
				
				Log.d("hello","qry:"+query);
				try {
				     output = urlConnection.getOutputStream();
				     output.write(query.getBytes(charset));
				} finally {
				     if (output != null) try { output.close(); } catch (IOException logOrIgnore) {}
				}
				
				urlConnection.setReadTimeout(5000);
				
				urlConnection.connect();

				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

				String responseBody = bufferedReader.readLine();
				
				if(responseBody==null){
					Log.d("hello","null resp");
					unregStatus=false;
				}
				else if (responseBody.indexOf("false")!=-1){
					Log.d("hello","resp-"+responseBody);
					unregStatus=false;
				} else unregStatus=true;
				
			} catch (MalformedURLException e) {
				publishProgress("Url");
			} catch (IOException e) {
				publishProgress("Timeout");
			} catch (NullPointerException e) {
				publishProgress("Null");
			}
		}

		protected void onProgressUpdate(String... values) {
				Log.d("hello", values[0]);
				Toast.makeText(getBaseContext(), values[0], Toast.LENGTH_SHORT).show();
		}
	}
	
	int getCurrentDistance(int location) {
		return location * 10;
	}
	
	void getEventsList(){
		eventslist = new ArrayList<Map<String, String>>();
		for (int i = 0; i < newname.length; i++) {
			try {
				Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).parse(newstarttime[i]);
			
				if(!(newname[i].trim().equals(""))) {					
					Map<String, String> datum = new HashMap<String, String>(2);
					datum.put("Name", newname[i]);
					datum.put("Desc", "Location:\t" + locationNames[newloc[i]] + "\n"
							+ "Duration:\t" + String.format("%.1f", ((float)newduration[i])/60.0f) + " hours\n"
							+ "Start Time:\t" +  new SimpleDateFormat("MMM dd KK:mm a").format(date) + "\n");
					eventslist.add(datum);
				}
			} catch (Exception e) {
				Log.e("Conversion", e.toString());
			}
		}
		
		ListView listview = (ListView) findViewById(R.id.listView1);
		SimpleAdapter adapter = new SimpleAdapter(this, eventslist,
				android.R.layout.simple_list_item_2, new String[] { "Name",
						"Desc" }, new int[] { android.R.id.text1,
						android.R.id.text2 });
		listview.setAdapter(adapter);
		
		listview.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int pos, long id) {
				
				position=pos;
				final String item = newname[position];
				
				AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(MyEvents.this);
				dlgAlert.setTitle("Unregister this event?");
				dlgAlert.setMessage(item);
				
				dlgAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() 
				{
			        public void onClick(DialogInterface dialog, int which)
			        {
			        	int rp=0;
						for (int j = 0; j < e.size(); j++) {
							if(item.equals(e.get(j).Name))
								rp=e.get(j).EventID;
						}
			        	new UnregEvent().execute(rp);
			        	
			          }});
				
				dlgAlert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() 
				{
			        public void onClick(DialogInterface dialog, int which)
			        {
			            
			          }});			
				
				dlgAlert.create().show();

//				Toast.makeText(getBaseContext(), item, Toast.LENGTH_SHORT).show();
			}
		});
	}
	
	private void loadMyEvents() {
    	SharedPreferences settings = getSharedPreferences(PREFS_MYEVENTS, 0);
    	myEventsString=settings.getString("myEventsString", null);
    	if(myEventsString!=null)
    		myEvents = myEventsString.split(";");
    	else
    		myEvents = null;
//    	else{
//    		myEvents = new String[1];
//    		myEvents[0]="Closing ceremony";
//    	}
	}
	
	@Override
	public void onStop(){
		super.onStop();
		if(myEventsString!=null){
			SharedPreferences settings = getSharedPreferences(PREFS_MYEVENTS, 0);
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("myEventsString", myEventsString);
			editor.commit();
		}
	}
	
}
