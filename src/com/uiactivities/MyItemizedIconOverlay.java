package com.uiactivities;

import static com.uiactivities.MapActivity.e;
import static com.uiactivities.MapActivity.finalLocation;
import static com.uiactivities.MapActivity.locationNames;

import java.util.ArrayList;
import java.util.List;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MyItemizedIconOverlay implements OnClickListener {
	protected ItemizedIconOverlay<OverlayItem> mOverlay;
	protected Context mContext;
	protected Drawable mMarker;
	private Dialog dialog1;
	OverlayItem it;
	ArrayAdapter<String> adapter ;
	ListView listview;
	public Dialog regdialog=null;
	public Activity Act;
	
	List<String> eventsHere;
	private int locpos;
	//	List<Integer> q1;
	
	public MyItemizedIconOverlay(Drawable marker, Context context, Activity a) {	
		mContext = context;
		Act=a;
		ArrayList<OverlayItem> items = new ArrayList<OverlayItem>();
		ResourceProxy resourceProxy = (ResourceProxy) new DefaultResourceProxyImpl(mContext);
		mMarker = marker;

		mOverlay = new ItemizedIconOverlay<OverlayItem>(
			items, 
			mMarker, 
			new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
				@Override 
				public boolean onItemSingleTapUp(final int index, final OverlayItem item) {
					return onSingleTapUpHelper(index, item);
				}

				@Override 
				public boolean onItemLongPress(final int index, final OverlayItem item) {
					return true;
				}
			}, 
			resourceProxy);
		}
	
	public boolean onSingleTapUpHelper(int i, OverlayItem item) {
		it=item;
		dialog1 = new Dialog(mContext);
		
		dialog1.setContentView(R.layout.activity_loctap);
    	dialog1.setTitle(item.getTitle());
    	dialog1.setCancelable(true);
    	locpos = Integer.parseInt(it.getSnippet());
    	Log.d("Update", " " + Integer.parseInt(it.getSnippet()) + " " + i);
    	
    	if(item.getTitle().equals("Current Location")){
    		dialog1.setTitle(locationNames[locpos]);
    		TextView b=(TextView) dialog1.findViewById(R.id.button1);
    		b.setText("Your "+item.getTitle()+".");
    		b.setEnabled(false);
    		Log.d("Update", "Current loc tap " + Integer.parseInt(it.getSnippet()) + " " + i);
    		dialog1.show();
    	}
    	else if(item.getTitle().equals("Destination")){
    		dialog1.setTitle(locationNames[locpos]);
    		TextView b=(TextView) dialog1.findViewById(R.id.button1);
    		b.setText("Your "+item.getTitle()+".");
    		b.setEnabled(false);
    		Log.d("Update", "Final loc tap " + Integer.parseInt(it.getSnippet()) + " " + i);
    		dialog1.show();
    	}

    	listview = (ListView) dialog1.findViewById(R.id.listView1);
    	
    	eventsHere=new ArrayList<String>();
    	
    	for(int j=0;j<e.size();j++)
    		if(e.get(j).LocationID==locpos)
    			eventsHere.add(e.get(j).Name);
    	
    	if(eventsHere.isEmpty())
    		eventsHere.add("No events here. Come back later.");
    	
		adapter = new ArrayAdapter<String>(mContext,
				android.R.layout.simple_list_item_1, eventsHere);
		listview.setAdapter(adapter);

		if(!eventsHere.get(0).equals("No events here. Come back later."))
		listview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int pos, long id) {
				String newname = ((TextView) view).getText().toString();
				
				regdialog = new Dialog(mContext);
				
				int rp=0;
				for (int j = 0; j < e.size(); j++) {
					if(newname.equals(e.get(j).Name))
						rp=j;
				}
				
//				enter(regdialog,rp);
				RegisterReminder r= new RegisterReminder();
				r.enter(regdialog, rp, Act);
				
//				Toast.makeText(mContext, newname, Toast.LENGTH_SHORT).show();
			}
		});
    	
    	dialog1.show();
    	
    	View b1=dialog1.findViewById(R.id.button1);
		b1.setOnClickListener(this);
		
		return true;
	}
	
	public boolean addItem(OverlayItem item){
		return mOverlay.addItem(item);
	}
	
	public ItemizedIconOverlay<OverlayItem> getOverlay(){
		return mOverlay;
	}
	
	public void onClick(View v){
		switch(v.getId()){
			case R.id.button1:
				finalLocation = MapActivity.locationXYtoMap[Integer.parseInt(it.getSnippet())] - 1;
				
				((MapActivity) Act).onPositionChanged();
				dialog1.dismiss();
				break;
		}
	}
}
