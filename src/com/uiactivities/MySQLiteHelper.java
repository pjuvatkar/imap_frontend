package com.uiactivities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper{
	
	//private static String DB_PATH = "/data/data/YOUR_PACKAGE/databases/";
	private static String DB_PATH = Environment.getDataDirectory() + "/data/com.gridants.gridnav/databases/";
	 
    private String DB_NAME = "";
 
    private SQLiteDatabase myDataBase; 
    
    private Context context;
	
	// Constructor
	public MySQLiteHelper(Context context, String name) {
		// For now, database version is fixed
		super(context, name, null, DATABASE_VERSION);
		Log.d("Database","MySQLiteHelper constructor");
		DB_NAME = name;
		this.context = context; 
	}
	
	private static final int DATABASE_VERSION = 1;
	
	// Table Readings
	public static final String TABLE_READINGS = "readings";

	// Table readings Columns names
	public static final String COLUMN_LOCATION = "location";
	public static final String COLUMN_READINGNUM = "readingnum";
	public static final String COLUMN_ROUTERID = "routerid";
	public static final String COLUMN_LEVEL = "level";
	
	// Table routers
	public static final String TABLE_ROUTERS = "routers";
	
	// Table routers Columns names
	public static final String COLUMN_BSSID = "bssid";
	// column for routers table, routerid is already defined
	
	// Table map
	public static final String TABLE_MAP = "map";
	
	// Table map column names
	public static final String COLUMN_XLAT = "xlat";
	public static final String COLUMN_YLON = "ylon";
	// A column for map table, location is already defined
	
	// Table path
	public static final String TABLE_PATH = "path";
	
	public static final String COLUMN_ID = "id";
	// It has columns COLUMN_XLAT and COLUMN_YLON
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.d("Database","Created routers table");
		
		// Create readings table
		String CREATE_READINGS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_READINGS + "("
				+ COLUMN_LOCATION + " INT," + COLUMN_READINGNUM + " INT," + COLUMN_ROUTERID + " INT," +  COLUMN_LEVEL + " INT" +" )";
		db.execSQL(CREATE_READINGS_TABLE);
		Log.d("Database","Created readings table");
		
		// create routers table
		String CREATE_ROUTERS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_ROUTERS + " (" 
				+ COLUMN_ROUTERID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_BSSID +" text)";
		db.execSQL(CREATE_ROUTERS_TABLE);
		
		// create map table
		String CREATE_MAP_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_MAP + " (" 
				+ COLUMN_LOCATION + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_XLAT +" INT, " + COLUMN_YLON +" INT)";
		db.execSQL(CREATE_MAP_TABLE);
		
		// create path table
		String CREATE_PATH_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_PATH + " (" 
				+ COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_XLAT +" INT, " + COLUMN_YLON +" INT)";
		db.execSQL(CREATE_PATH_TABLE);
	}
	
	public void createDataBase() throws IOException{
		 
    	boolean dbExist = checkDataBase();
 
    	if (dbExist) {
    		//do nothing - database already exist
    	} else {
 
    		//By calling this method and empty database will be created into the default system path
               //of your application so we are gonna be able to overwrite that database with our database.
        	this.getReadableDatabase();
 
        	try {
 
    			copyDataBase();
 
    		} catch (IOException e) {
 
        		throw new Error("Error copying database");
 
        	}
    	}
    }
	
	private boolean checkDataBase(){
		 
    	SQLiteDatabase checkDB = null;
 
    	try{
    		String myPath = DB_PATH + DB_NAME;
    		checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
 
    	}catch(SQLiteException e){
 
    		//database does't exist yet.
 
    	}
 
    	if(checkDB != null){
 
    		checkDB.close();
 
    	}
 
    	return checkDB != null ? true : false;
    }
	
	private void copyDataBase() throws IOException{
		
    	InputStream myInput = context.getAssets().open(DB_NAME);

    	if(myInput != null) {
    		// Path to the just created empty db
	    	String outFileName = DB_PATH + DB_NAME;
	 
	    	//Open the empty db as the output stream
	    	OutputStream myOutput = new FileOutputStream(outFileName);
	 
	    	//transfer bytes from the inputfile to the outputfile
	    	byte[] buffer = new byte[1024];
	    	int length;
	    	while ((length = myInput.read(buffer))>0){
	    		myOutput.write(buffer, 0, length);
	    	}
	 
	    	//Close the streams
	    	myOutput.flush();
	    	myOutput.close();
	    	myInput.close();
		} else {
			Log.d("Database","Input file not found in SD card");
		}		
    }
 
    public SQLiteDatabase openDataBase() throws SQLException{
 
    	//Open the database
        String myPath = DB_PATH + DB_NAME;
    	myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
    	return myDataBase;
 
    }
    @Override
	public synchronized void close() {
 
    	    if(myDataBase != null)
    		    myDataBase.close();
 
    	    super.close();
 
	}
	
    public boolean deleteDataBase() throws IOException {
		String DBFileName = DB_PATH + DB_NAME;
		File DBFile = new File(DBFileName);
		return DBFile.delete();
	}
    
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		onCreate(db);
	}

}
