package com.uiactivities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Register extends Activity implements OnClickListener {

	public TextView  emsg;
	public Button submit;
	public EditText uname, pwd, email, mobile, repwd;
	private MenuItem Settings;
	public boolean auth=false;
	public String u,p,m,e, rep;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reg);

		uname = (EditText) findViewById(R.id.userName);
		pwd = (EditText) findViewById(R.id.password);
		mobile = (EditText) findViewById(R.id.mobileNo);
		email = (EditText) findViewById(R.id.email);
		repwd = (EditText) findViewById(R.id.password2);
		
		submit = (Button) findViewById(R.id.submitregbutton);
		submit.setOnClickListener(this);
		emsg = (TextView) findViewById(R.id.errmsg);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		super.onCreateOptionsMenu(menu);
		Settings = menu.add("Settings");
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item == Settings){
			Intent i=new Intent(this, Setting.class);
			startActivity(i);
		}	
		return true;
	}
	
	public void onClick(View v) {

		if (v.getId() == R.id.submitregbutton) {

			u = uname.getText().toString();
			p = pwd.getText().toString();
			rep = repwd.getText().toString();
			m = mobile.getText().toString();
			e = email.getText().toString();
			
			if(u.equals("")){
				uname.requestFocus();
				Toast.makeText(getApplicationContext(), "Name cannot be empty.", Toast.LENGTH_SHORT).show();
				emsg.setText("Name cannot be empty.");
			}
			else if(e.equals("")){
				email.requestFocus();
				Toast.makeText(getApplicationContext(), "Email cannot be empty.", Toast.LENGTH_SHORT).show();
				emsg.setText("Email cannot be empty.");
			}
			else if(m.equals("")){
				mobile.requestFocus();
				Toast.makeText(getApplicationContext(), "Mobile cannot be empty.", Toast.LENGTH_SHORT).show();
				emsg.setText("Mobile cannot be empty.");
			}
			else if(p.equals("")){
				pwd.requestFocus();
				Toast.makeText(getApplicationContext(), "Password cannot be empty.", Toast.LENGTH_SHORT).show();
				emsg.setText("Password cannot be empty.");
			}
			else if(rep.equals("")){
				repwd.requestFocus();
				Toast.makeText(getApplicationContext(), "Retype Password.", Toast.LENGTH_SHORT).show();
				emsg.setText("Retype Password.");
			}
			else if(!rep.equals(p)){
				repwd.requestFocus();
				Toast.makeText(getApplicationContext(), "Passwords don't match.", Toast.LENGTH_SHORT).show();
				emsg.setText("Passwords don't match.");
			}
			else if(!checkMob()){
				mobile.requestFocus();
			}
			else {
				// can do some local validation as necessary and post to server through the DoReg fn which is invoked as follows
				new DoReg().execute();
				emsg.setText("Registering. Please wait.");
			}
			
			
		} else {

		}
	}
	
	boolean checkMob(){
		try{
			Double.parseDouble(m);
		} catch (NumberFormatException e){
			Toast.makeText(getApplicationContext(), "Not a valid mobile number.", Toast.LENGTH_SHORT).show();
			emsg.setText("Not a valid mobile number.");
			return false;
		}
		if(m.length()<6){
			Toast.makeText(getApplicationContext(), "Not a valid mobile number.", Toast.LENGTH_SHORT).show();
			emsg.setText("Mobile number and country code.");
			return false;
		}
		return true;
	}
	
	private class DoReg extends AsyncTask<String, String, Integer> {

		protected Integer doInBackground(String... arg) {
			register();
			return 0;
		}

		protected void onPostExecute(Integer arg) {
			if (auth) {
				publishProgress("Successfully registered. Please check email.");
				emsg.setText("Registered. Please check email.");
			} else {
				publishProgress("Duplicate Email Id or Mobile.");
			}
		}
		
		private void register() {
			try {
				publishProgress("Connecting...");
				URL url = new URL("https://www.sentimeter.me/imap/store_user.php");
				HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
				String charset = "UTF-8";
				urlConnection.setRequestMethod("POST");
				urlConnection.setDoOutput(true); // Triggers POST.
				urlConnection.setDoInput(true);
				urlConnection.setRequestProperty("Accept-Charset", charset);
				urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);
				OutputStream output = null;

				String param1 = u;
				String param2 = p;
				String param3 = m;
				String param4 = e;
				String query = String.format("FullName=%s&password=%s&MobileNumber=%s&EmailID=%s", 
				     URLEncoder.encode(param1, charset), 
				     URLEncoder.encode(param2, charset),
				     URLEncoder.encode(param3, charset),
				     URLEncoder.encode(param4, charset));
				
//				Log.d("hello","qry: "+query);
				try {
				     output = urlConnection.getOutputStream();
				     output.write(query.getBytes(charset));
				} finally {
				     if (output != null) try { output.close(); } catch (IOException logOrIgnore) {}
				}
				
				urlConnection.setReadTimeout(8000);
				
				urlConnection.connect();

				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

				String responseBody = bufferedReader.readLine();
				
				if(responseBody==null||responseBody.indexOf("false")!=-1){
					Log.d("hello","null resp");
					auth=false;
				}
				else if(responseBody.indexOf("true")!=-1){
					Log.d("hello",responseBody);
					auth=true;
				}
				
//				try{
//					String jsonstring=responseBody;
//					JSONObject jsonObject;
//					jsonObject = new JSONObject(jsonstring);
//					Iterator<?> keys = jsonObject.keys();
//					while (keys.hasNext()) {
//						String key = (String) keys.next();
//						if (jsonObject.getString(key).equals("true")) {
//							auth = true;
//						} else {
//							auth = false;
//						}
//
//					}
//				} catch(JSONException e) {
//					Log.d("hello","json ex");
//				}
				
			} catch (MalformedURLException e) {
//				Log.d("hello","url err");
			} catch (IOException e) {
				publishProgress("Timeout");
			}
		}
		
		protected void onProgressUpdate(String... values) {
			Log.d("hello", values[0]);
			Toast.makeText(getApplicationContext(), values[0], Toast.LENGTH_SHORT).show();
	}
}
}
