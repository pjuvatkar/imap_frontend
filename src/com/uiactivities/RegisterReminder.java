package com.uiactivities;

import static com.uiactivities.EventDetails.q1;
import static com.uiactivities.MapActivity.e;
import static com.uiactivities.MapActivity.locationNames;
import static com.uiactivities.MapActivity.myEventsString;
import static com.uiactivities.MyEvents.PREFS_MYEVENTS;
import static com.uiactivities.Signin.PREFS_NAME;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterReminder {

Activity Act;
boolean regStatus=false;
int p;
String eventname=null;

public void enter(final Dialog dialog, final int pos, Activity a){
	Act=a;
	p=pos;
	if(q1==null)
		q1=new ArrayList<Integer>();
	dialog.setContentView(R.layout.dialog_eventregister);
	dialog.setTitle("Event Registration");
	dialog.setCancelable(true);
	TextView t=(TextView) dialog.findViewById(R.id.name);
	t.setText(e.get(pos).Name);
	t=(TextView) dialog.findViewById(R.id.loc);
	t.setText(locationNames[e.get(pos).LocationID]);
	t=(TextView) dialog.findViewById(R.id.duration);
	t.setText(String.format("%.1f hours", ((float)e.get(pos).Duration)/60.0f));
	t=(TextView) dialog.findViewById(R.id.desc);
	t.setText(""+e.get(pos).Description);
	dialog.show();
	View b1=dialog.findViewById(R.id.done);
	b1.setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			regEvent(e.get(pos).Name);
			CheckBox evRem = (CheckBox) dialog.findViewById(R.id.checkBoxSetRem);
			if(evRem.isChecked())
				rem(pos);
			dialog.cancel();
		}
	});
	View b2=dialog.findViewById(R.id.cancel);
	b2.setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			dialog.dismiss();
		}
	});
}

private void regEvent(String ename) {
	eventname=ename;
	new RegEvent().execute();
}

public void rem(int pos){
//	Calendar cal = Calendar.getInstance();     
	Uri EVENTS_URI = Uri.parse(getCalendarUriBase(Act) + "events");
	ContentResolver cr = Act.getContentResolver();

	// event insert
	ContentValues values = new ContentValues();
	values.put("calendar_id", 1);
	values.put("title", e.get(pos).Name);
	values.put("allDay", 0);
	DateFormat myDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
	Date tempDate = null;
	try {
		tempDate = myDateFormat.parse(e.get(pos).StartTime);
		Log.d("datedrem",tempDate.toString());
	} catch (ParseException e1) {
		Log.d("datedrem","parse fail");
	}
	values.put("dtstart", tempDate.getTime()); // event starts at startTime
	values.put("dtend", tempDate.getTime()+e.get(pos).Duration*1000*60); // ends duration minutes from then
	values.put("description", "Description: " + e.get(pos).Description +".");
	//values.put("visibility", 0);
	values.put("hasAlarm", 1);
	values.put("eventTimezone", "Kolkata");
	Uri event = cr.insert(EVENTS_URI, values);

	// reminder insert
	Uri REMINDERS_URI = Uri.parse(getCalendarUriBase(Act) + "reminders");
	values = new ContentValues();
	values.put( "event_id", Long.parseLong(event.getLastPathSegment()));
	values.put( "method", 1 );
	values.put( "minutes", 10 );
	cr.insert( REMINDERS_URI, values );
	
	q1.add(pos);
//	setLocalRem(8000);
}

//protected Handler taskHandler = new Handler();
//Runnable t;
//public void setLocalRem(long time) {
//	final long elapse = time;
//	t = new Runnable() {
//		public void run() {
//			EventAlert();
//		}
//	};
//	taskHandler.postDelayed(t, elapse);
//}
//
//protected void EventAlert() {
//	Toast.makeText(Act.getBaseContext(), "Rem....", Toast.LENGTH_SHORT).show();
//	
//	Intent i = new Intent(Act.getBaseContext(), Reminder.class);
//	Act.startActivity(i);
////	remDialog(dialog, 0);
//	Log.d("hello","rem");
//}

private String getCalendarUriBase(Activity act) {

    String calendarUriBase = null;
    Uri calendars = Uri.parse("content://calendar/calendars");
    Cursor managedCursor = null;
    try {
        managedCursor = act.managedQuery(calendars, null, null, null, null);
    } catch (Exception e) {
    }
    if (managedCursor != null) {
        calendarUriBase = "content://calendar/";
    } else {
        calendars = Uri.parse("content://com.android.calendar/calendars");
        try {
            managedCursor = act.managedQuery(calendars, null, null, null, null);
        } catch (Exception e) {
        }
        if (managedCursor != null) {
            calendarUriBase = "content://com.android.calendar/";
        }
    }
    return calendarUriBase;
}

class RegEvent extends AsyncTask<String, String, Integer> {

	protected Integer doInBackground(String... arg) {
		registerEvent();
		return 0;
	}

	protected void onPostExecute(Integer arg) {
		if (regStatus) {
			publishProgress("Reg Successful");
			if(regStatus){
				if(myEventsString==null)
					myEventsString=eventname;
				else if(myEventsString.indexOf(eventname)!=-1)
					return;
				else
					myEventsString=myEventsString+";"+eventname;
				if(myEventsString!=null){
					SharedPreferences settings = Act.getSharedPreferences(PREFS_MYEVENTS, 0);
					SharedPreferences.Editor editor = settings.edit();
					editor.putString("myEventsString", myEventsString);
					editor.commit();
				}
			}
		} else {
			publishProgress("Reg Failed");
		}
	}
	
	private void registerEvent() {
		try {
			regStatus=false;
			publishProgress("Connecting...");
			URL url = new URL("https://www.sentimeter.me/imap/store_userevent.php");
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			String charset = "UTF-8";
			urlConnection.setRequestMethod("POST");
			urlConnection.setDoOutput(true); // Triggers POST.
			urlConnection.setDoInput(true);
			urlConnection.setRequestProperty("Accept-Charset", charset);
			urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);
			OutputStream output = null;

			SharedPreferences settings = Act.getSharedPreferences(PREFS_NAME, 0);
			int a=Integer.parseInt(settings.getString("UserID", "-1"));
			
			String query = "UserID="+a+"&EventID="+e.get(p).EventID;
			
			Log.d("hello","qry:"+query);
			try {
			     output = urlConnection.getOutputStream();
			     output.write(query.getBytes(charset));
			} finally {
			     if (output != null) try { output.close(); } catch (IOException logOrIgnore) {}
			}
			
			urlConnection.setReadTimeout(5000);
			
			urlConnection.connect();

			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

			String responseBody = bufferedReader.readLine();
			
			if(responseBody==null){
				Log.d("hello","null resp");
				return;
			}
			else {
				Log.d("hello","resp-"+responseBody);
			}
				
			if(responseBody.indexOf("true")!=-1)
				regStatus=true;
			else if(responseBody.indexOf("Duplicate")!=-1)
				publishProgress("Already registered.");
			else regStatus=false;
				
		} catch (MalformedURLException e) {
		} catch (IOException e) {
			publishProgress("Timeout");
		}
	}

	protected void onProgressUpdate(String... values) {
			Log.d("hello", values[0]);
			Toast.makeText(Act.getApplicationContext(), values[0], Toast.LENGTH_SHORT).show();
	}
}
}
