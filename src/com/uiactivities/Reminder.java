package com.uiactivities;

import static com.uiactivities.MapActivity.e;
import static com.uiactivities.MapActivity.finalLocation;
import static com.uiactivities.MapActivity.locationNames;
import static com.uiactivities.MapActivity.mapActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class Reminder extends Activity implements OnClickListener {

	int p;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_reminder);
		p=EventDetails.q1.get(0);
		EventDetails.q1.remove(0);
		TextView t=(TextView) findViewById(R.id.name);
    	t.setText(e.get(p).Name);
    	t=(TextView) findViewById(R.id.loc);
    	t.setText(locationNames[e.get(p).LocationID]);
    	t=(TextView) findViewById(R.id.duration);
    	t.setText(e.get(p).Duration+"");
    	t=(TextView) findViewById(R.id.desc);
    	t.setText("Desc "+e.get(p).Description);
    	View b1=findViewById(R.id.path);
		b1.setOnClickListener(this);
		View b2=findViewById(R.id.dismiss);
		b2.setOnClickListener(this);
	}
	
	public void onClick(View v){
		switch(v.getId()){
			case R.id.path:
				finalLocation=e.get(p).LocationID;
				((MapActivity) mapActivity).onPositionChanged();
				Intent i = new Intent(this, MapActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(i);
				finish();
				break;
			case R.id.dismiss:
				finish();
				break;
		}
	}
}
