package com.uiactivities;

import static com.uiactivities.MapActivity.finalLocation;
import static com.uiactivities.MapActivity.locationNames;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SearchLoc extends Activity {
	
	String searchfld;
	ArrayAdapter<String> adapter ;
	ListView listview;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_searchloc);
		
		listview = (ListView) findViewById(R.id.listView1);
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, locationNames);
		listview.setAdapter(adapter);

		listview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int pos, long id) {
				String item = ((TextView) view).getText().toString();
				
				finalLocation = MapActivity.locationXYtoMap[searchLocationId(listview.getItemAtPosition(pos).toString())]-1;
				
				Toast.makeText(getBaseContext(), item, Toast.LENGTH_SHORT).show();
				finish();
			}
		});
		
		final EditText search = (EditText) findViewById(R.id.searchbar);
		final List<String> list = new ArrayList<String>();
		search.addTextChangedListener(new TextWatcher(){
	        public void afterTextChanged(Editable s) {
	        	searchfld=search.getText().toString();
	        	list.clear();
	        	for(int i=0;i<locationNames.length;i++){
	        		if(locationNames[i].toLowerCase(Locale.getDefault()).indexOf(searchfld.toLowerCase())!=-1)
	        			list.add(locationNames[i]);
	        	}
	        	if(list.isEmpty())
	        		list.add("Nothing matches found.");
	        	adapter = new ArrayAdapter<String>(SearchLoc.this, android.R.layout.simple_list_item_1, list);
	        	listview.setAdapter(adapter);
	        }
	        public void beforeTextChanged(CharSequence s, int start, int count, int after){}
	        public void onTextChanged(CharSequence s, int start, int before, int count){}
	    });
		
	}
	
	static int searchLocationId(String locationName) {
		for (int i = 0; i < MapActivity.locationNames.length; i++) {
			if(MapActivity.locationNames[i].equals(locationName)) {
				return i;
			}
		}
		return 0;
	}
}
