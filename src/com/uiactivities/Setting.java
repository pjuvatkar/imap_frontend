package com.uiactivities;

import static com.uiactivities.Signin.PREFS_NAME;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class Setting extends Activity {

	public static boolean turnOnLoc=false;
//	CheckBox locchkbox;
	
	String getUserProfile()
	{
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		settings.getString("UserId",null);
		
		String s = "Name: " + settings.getString("FirstName",null) + " " 
				+ settings.getString("LastName",null) + "\n" 
				+ "Email ID: " + settings.getString("EmailId",null) + "\n" 
				+ "Mobile Number: " + settings.getString("MobileNoCode",null) + "" 
				+ settings.getString("MobileNo",null); 
		
		return s;
	}
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		List<String> list = new ArrayList<String>();
		list.add("Profile");
		list.add("How to use");
	//	list.add("Terms");
		list.add("About VG 2013");
		list.add("About App"); // or whatever required

		ListView listview = (ListView) findViewById(R.id.listView1);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(Setting.this,
				android.R.layout.simple_list_item_1, list);
		listview.setAdapter(adapter);

		listview.setOnItemClickListener(new OnItemClickListener() {
			Dialog dialog;

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				String item = ((TextView) view).getText().toString();

				//Toast.makeText(getBaseContext(), item, Toast.LENGTH_SHORT).show();
				dialog = new Dialog(Setting.this);
				dialog.setContentView(R.layout.dialog_settings);
		    	dialog.setTitle(item);
		    	dialog.setCancelable(true);
		    	TextView t=(TextView)dialog.findViewById(R.id.settingsdialogcontent);
		    	
		    	if(item=="About VG 2013")
		    	{
		    		t.setText(item+"\n"+"The Vibrant Gujarat Summit has become an example of" +
		    				" visionary approach of the State Government to investment promotion and " +
		    				"advancement of economic and social development for many states. The event " +
		    				"provides enormous prospects to the State to display its strengths, " +
		    				"progressive stand, initiatives taken to improve governance, investor friendly" +
		    				" climate and art and culture of Gujarat.For more details go to " +
		    				"www.vibrantgujarat.com");
		    		dialog.show();
		    	}
		    	else if(item=="About App")
		    	{
		    		t.setText(item+"\n"+"The application has been developed by GridAnts " +
		    				"(www.gridants.com) and is being distributed by iNDEXTb the organizers of " +
		    				"Vibrant Gujarat. The application provides location based information to users " +
		    				"allowing clear and successful navigation inside the venue during Vibrant " +
		    				"Gujarat 2013.");
		    		dialog.show();
		    	}
		    	
		    	else if(item=="How to use")
		    	{
		    		
		    		Intent i=new Intent(Setting.this, Demo.class);
					startActivity(i);
		    	}
		    	
		    	else if(item=="Profile")
		    	{
		    		t.setText(getUserProfile());
		    		t.setTextAppearance(Setting.this, android.R.attr.textAppearanceLarge);
		    		dialog.show();
		    	}
		    	
		    	View b1=dialog.findViewById(R.id.canceldialog);
				b1.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						dialog.cancel();
					}
				});
			}
		});
	}

}
