package com.uiactivities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Signin extends Activity implements OnClickListener {

	public final static String PREFS_NAME = "LoginPrefsFile";
	public static String USER_NAME;
	public String DEF_U = "";
	public String DEF_MOBILE = "919876543210";
	public String PWD, DEF_P = "";
	public static boolean log = false;
	public boolean fetchStatus;
	public TextView user;
	public Button submit, logout;
	public EditText uname, pwd;
	private MenuItem Settings;
	public boolean auth=false;
	
	public String email=null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signin);
		
		uname = (EditText) findViewById(R.id.email);
		pwd = (EditText) findViewById(R.id.pwd);
		submit = (Button) findViewById(R.id.button1);
		submit.setOnClickListener(this);
		logout = (Button) findViewById(R.id.button2);
		logout.setOnClickListener(this);

		user = (TextView) findViewById(R.id.textView1);
		
	}
	
	@Override
    protected void onResume() {
		super.onResume();
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		log = settings.getBoolean("loggedin", false);

		if (log) {
			email=settings.getString("username", null);
			USER_NAME=settings.getString("username", null);
			
			user.setText("Signed in as " + email +".");
			submit.setEnabled(false);
			
			uname.setEnabled(false);
			pwd.setEnabled(false);
			uname.setText(email);
			pwd.setText("1111");
			
			fetchStatus = settings.getBoolean("userData", false);
			
			if(!fetchStatus)
				new FetchUserData().execute();
			
			if(fetchStatus){
				SharedPreferences.Editor editor = settings.edit();
				editor.putBoolean("userData", fetchStatus);

				editor.commit();
				Intent i=new Intent(this, MapActivity.class);
				startActivity(i);
				
			}
			
		} else{
			user.setText("Not signed in.");
		}
	}

	@Override
	protected void onStop() {
		super.onStop();

		// We need an Editor object to make preference changes.
		// All objects are from android.context.Context
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean("loggedin", log);
		if (log)
			editor.putString("username", USER_NAME);
		else
			editor.putString("username", null);
		// Commit the edits!
		editor.commit();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		super.onCreateOptionsMenu(menu);
		
		Settings = menu.add("Settings");
	
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item == Settings){
			Intent i=new Intent(this, Setting.class);
			startActivity(i);
		}	
		return true;
	}
	
	public void onClick(View v) {

		if (v.getId() == R.id.button1) {

			String u = uname.getText().toString();
			String p = pwd.getText().toString();

			if ((u.equals(DEF_U) || u.equals(DEF_MOBILE)) && p.equals(DEF_P)) {
				log = true;
				user.setText("Signed in as " + DEF_U + ".");
				
				USER_NAME = "";
				
				submit.setEnabled(false);
				uname.setEnabled(false);
				pwd.setEnabled(false);

				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putBoolean("loggedin", log);
				if (log)
					editor.putString("username", USER_NAME);
				else
					editor.putString("username", null);
				// Commit the edits!
				editor.commit();
			}

			else if (pwd.getText().toString().equals("") || uname.getText().toString().equals("")) {
				Toast.makeText(Signin.this, "Please enter username and password.", Toast.LENGTH_SHORT).show();
			} else if (!pwd.getText().toString().equals("") && !uname.getText().toString().equals("")) {
				USER_NAME = uname.getText().toString();
				Log.d("hello", "uname:" + USER_NAME);
				PWD = pwd.getText().toString();
				new DoLogin().execute();
			} else {
				log = false;
				user.setText("Not signed in.");
				submit.setEnabled(true);
				uname.setEnabled(true);
				pwd.setEnabled(true);
				uname.setText(null);
				pwd.setText(null);
				uname.requestFocus();
			}
			
		} else if (v.getId() == R.id.button2) {
			log=false;
			auth=false;
			uname.setEnabled(true);
			pwd.setEnabled(true);
			submit.setEnabled(true);
			uname.setText(null);
			pwd.setText(null);
			user.setText("Not signed in.");
			uname.requestFocus();
			
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			SharedPreferences.Editor editor = settings.edit();
			editor.putBoolean("loggedin", log);
			if (log) {
				editor.putString("username", USER_NAME);
			}
			else {
				editor.putString("username", null);
				fetchStatus=false;
				editor.putBoolean("userData", fetchStatus);
			}
			// Commit the edits!
			editor.commit();
		}
	}

	private class DoLogin extends AsyncTask<String, String, Integer> {

		protected Integer doInBackground(String... arg) {
			login();
			return 0;
		}

		protected void onPostExecute(Integer arg) {
			if (auth) {
				log=auth;
				publishProgress("Login successful");
//				usernameTxt.setEnabled(false);
//				passwordTxt.setEnabled(false);
//				loginBut.setEnabled(false);
				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putBoolean("loggedin", log);
				if (log) {
					editor.putString("username", USER_NAME);
				}
				else {
					editor.putString("username", null);
				}
				// Commit the edits!
				editor.commit();
				TextView loggedin = (TextView) findViewById(R.id.textView1);
				loggedin.setText(Html.fromHtml("Signed in as <b>"+settings.getString("username", null)+".</b>"));
				Intent i = new Intent(Signin.this.getBaseContext(), Signin.class);
				startActivity(i);
				
				finish();
			} else {
//				log=false;
				publishProgress("Invalid username/password");
			}
			Log.d("hello","postexec");
		}
		
		private void login() {
			try {
				publishProgress("Connecting for sign in...");
				URL url = new URL("https://www.sentimeter.me/imap/authenticate.php");
				HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
				String charset = "UTF-8";
				urlConnection.setRequestMethod("POST");
				urlConnection.setDoOutput(true); // Triggers POST.
				urlConnection.setDoInput(true);
				urlConnection.setRequestProperty("Accept-Charset", charset);
				urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);
				OutputStream output = null;

				String param1 = USER_NAME;
				String param2 = PWD;
				MessageDigest digest;
				try {
					digest = MessageDigest.getInstance("MD5");
					digest.reset();
					digest.update(PWD.getBytes());
					byte[] a = digest.digest();
					int len = a.length;
					StringBuilder sb = new StringBuilder(len << 1);
					for (int i = 0; i < len; i++) {
						sb.append(Character.forDigit((a[i] & 0xf0) >> 4, 16));
						sb.append(Character.forDigit(a[i] & 0x0f, 16));
					}
					param2 = sb.toString();
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				}
				    
				String query = String.format("EmailId=%s&Password=%s", 
				     URLEncoder.encode(param1, charset), 
				     URLEncoder.encode(param2, charset));
				
				Log.d("hello","qry:"+query);
				try {
				     output = urlConnection.getOutputStream();
				     output.write(query.getBytes(charset));
				} finally {
				     if (output != null) try { output.close(); } catch (IOException logOrIgnore) {}
				}
				
				urlConnection.setReadTimeout(5000);
				
				urlConnection.connect();

				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

				String responseBody = bufferedReader.readLine();
				
				if(responseBody==null)
					Log.d("hello","null resp");
				else {
					Log.d("hello","resp-"+responseBody);
				}
				
				try{
					String jsonstring=responseBody;
					JSONObject jsonObject;
					jsonObject = new JSONObject(jsonstring);
					Iterator<?> keys = jsonObject.keys();
					while (keys.hasNext()) {
						String key = (String) keys.next();
						if (jsonObject.getString(key).equals("1")) {
							auth = true;
						} else {
							auth = false;
						}
					}
				} catch(JSONException e) {
					Log.d("hello","json ex");
				}
				
//				urlConnection.disconnect();

//				publishProgress("Content updated.");
			} catch (MalformedURLException e) {
			} catch (IOException e) {
				publishProgress("Timeout");
			}
		}

		protected void onProgressUpdate(String... values) {
				Log.d("hello", values[0]);
				Toast.makeText(getApplicationContext(), values[0], Toast.LENGTH_SHORT).show();
		}
	}
	
	class FetchUserData extends AsyncTask<String, String, Integer> {

		protected Integer doInBackground(String... arg) {
			fetch();
			return 0;
		}

		protected void onPostExecute(Integer arg) {
			if (fetchStatus) {
				publishProgress("Fetch Successful");
				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putBoolean("userData", fetchStatus);

				editor.commit();
				Intent i = new Intent(Signin.this.getBaseContext(), Signin.class);
				startActivity(i);
				
				finish();
			} else {
				publishProgress("Fetch Failed");
			}
		}
		
		private void fetch() {
			try {
				fetchStatus=false;
				publishProgress("Fetching user data...");
				URL url = new URL("https://www.sentimeter.me/imap/showuserinfo.php");
				HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
				String charset = "UTF-8";
				urlConnection.setRequestMethod("POST");
				urlConnection.setDoOutput(true); // Triggers POST.
				urlConnection.setDoInput(true);
				urlConnection.setRequestProperty("Accept-Charset", charset);
				urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);
				OutputStream output = null;

				String query = "EmailId="+email;
				
//				Log.d("hello","qry:"+query);
				try {
				     output = urlConnection.getOutputStream();
				     output.write(query.getBytes(charset));
				} finally {
				     if (output != null) try { output.close(); } catch (IOException logOrIgnore) {}
				}
				
				urlConnection.setReadTimeout(5000);
				
				urlConnection.connect();

				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

				String responseBody = bufferedReader.readLine();
				
				if(responseBody==null)
					Log.d("hello","null resp");
				else {
//					Log.d("hello","fetch resp-"+responseBody);
				}
				
				if(responseBody.indexOf("[]")!=-1){
					fetchStatus=false;
					return;
				}
				fetchStatus=true;
				try{
					String jsonstring=responseBody;
					JSONObject jsonObject = new JSONObject(jsonstring);
//					int i=0;
					SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
					SharedPreferences.Editor editor = settings.edit();
						
						String a = jsonObject.getString("UserId");
						editor.putString("UserID", a);
//						Log.d("hello", "entry-"+a);
						
						a = jsonObject.getString("FirstName");
						editor.putString("FirstName", a);
//						Log.d("hello", "entry-"+a);
						
						a = jsonObject.getString("LastName");
						editor.putString("LastName", a);
//						Log.d("hello", "entry-"+a);
						
						a = jsonObject.getString("EmailId");
						editor.putString("EmailId", a);
//						Log.d("hello", "entry-"+a);
						
						a = jsonObject.getString("MobileNoCode");
						editor.putString("MobileNoCode", a);
//						Log.d("hello", "entry-"+a);
						
						a = jsonObject.getString("MobileNo");
						editor.putString("MobileNo", a);
//						Log.d("hello", "entry-"+a);
					editor.commit();
				} catch(JSONException e) {
					Log.d("hello","json ex");
				}
				
			} catch (MalformedURLException e) {
			} catch (IOException e) {
				publishProgress("Timeout");
			}
		}

		protected void onProgressUpdate(String... values) {
				Log.d("hello", values[0]);
				Toast.makeText(getBaseContext(), values[0], Toast.LENGTH_SHORT).show();
		}
	}
}
